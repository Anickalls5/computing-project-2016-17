/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectPackage.MODEL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam
 */
public class DigitsTest {
    
    public DigitsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of convertINTtoSTRING method, of class Digits.
     */
    @Test
    public void testConvertINTtoSTRING() {
        System.out.println("convertINTtoSTRING");
        int input = 100;
        String expResult = "100";
        String result = Digits.convertINTtoSTRING(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertSTRINGtoINT method, of class Digits.
     */
    @Test
    public void testConvertSTRINGtoINT() {
        System.out.println("convertSTRINGtoINT");
        String input = "1000";
        int expResult = 1000;
        int result = Digits.convertSTRINGtoINT(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDOUBLEtoSTRING method, of class Digits.
     */
    @Test
    public void testConvertDOUBLEtoSTRING() {
        System.out.println("convertDOUBLEtoSTRING");
        double input = 2.5;
        String expResult = "2.5";
        String result = Digits.convertDOUBLEtoSTRING(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertSTRINGtoDOUBLE method, of class Digits.
     */
    @Test
    public void testConvertSTRINGtoDOUBLE() {
        System.out.println("convertSTRINGtoDOUBLE");
        String input = "5.75";
        double expResult = 5.75;
        double result = Digits.convertSTRINGtoDOUBLE(input);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of convertLONGtoSTRING method, of class Digits.
     */
    @Test
    public void testConvertLONGtoSTRING() {
        System.out.println("convertLONGtoSTRING");
        long input = 5000L;
        String expResult = "5000";
        String result = Digits.convertLONGtoSTRING(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertSTRINGtoLONG method, of class Digits.
     */
    @Test
    public void testConvertSTRINGtoLONG() {
        System.out.println("convertSTRINGtoLONG");
        String input = "7500";
        long expResult = 7500L;
        long result = Digits.convertSTRINGtoLONG(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of countDigits method, of class Digits.
     */
    @Test
    public void testCountDigits_int() {
        System.out.println("countDigits");
        int input = 123456;
        int expResult = 6;
        int result = Digits.countDigits(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of countDigits method, of class Digits.
     */
    @Test
    public void testCountDigits_double() {
        System.out.println("countDigits");
        double input = 789.10;
        int expResult = 4;
        int result = Digits.countDigits(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of countDigits method, of class Digits.
     */
    @Test
    public void testCountDigits_String() {
        System.out.println("countDigits");
        String input = "9876";
        int expResult = 4;
        int result = Digits.countDigits(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of isPrime method, of class Digits.
     */
    @Test
    public void testIsPrime_int() {
        System.out.println("isPrime");
        int input = 104869;
        boolean expResult = true;
        boolean result = Digits.isPrime(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of isPrime method, of class Digits.
     */
    @Test
    public void testIsPrime_long() {
        System.out.println("isPrime");
        long input = 100030001L;
        boolean expResult = true;
        boolean result = Digits.isPrime(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of splitDecimal method, of class Digits.
     */
    @Test
    public void testSplitDecimal_double() {
        System.out.println("splitDecimal");
        double input = 5.5;
        String[] expResult = {"5","5"};
        String[] result = Digits.splitDecimal(input);
        assertArrayEquals(expResult, result);

    }

    /**
     * Test of splitDecimal method, of class Digits.
     */
    @Test
    public void testSplitDecimal_String() {
        System.out.println("splitDecimal");
        String input = "7.75";
        String[] expResult = {"7","75"};
        String[] result = Digits.splitDecimal(input);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of trimDecimal method, of class Digits.
     */
    @Test
    public void testTrimDecimal_double_int() {
        System.out.println("trimDecimal");
        double input = 0.5356;
        int accuracy = 2;
        String expResult = "0.53";
        String result = Digits.trimDecimal(input, accuracy);
        assertEquals(expResult, result);
    }

    /**
     * Test of trimDecimal method, of class Digits.
     */
    @Test
    public void testTrimDecimal_String_int() {
        System.out.println("trimDecimal");
        String input = "10.777777";
        int accuracy = 3;
        String expResult = "10.777";
        String result = Digits.trimDecimal(input, accuracy);
        assertEquals(expResult, result);
    }

    /**
     * Test of roundDecimal method, of class Digits.
     */
    @Test
    public void testRoundDecimal_double_int() {
        System.out.println("roundDecimal");
        double input = 25.3346;
        int accuracy = 3;
        String expResult = "25.335";
        String result = Digits.roundDecimal(input, accuracy);
        assertEquals(expResult, result);
    }

    /**
     * Test of roundDecimal method, of class Digits.
     */
    @Test
    public void testRoundDecimal_String_int() {
        System.out.println("roundDecimal");
        String input = "100.11";
        int accuracy = 1;
        String expResult = "100.1";
        String result = Digits.roundDecimal(input, accuracy);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertINTtoBASE method, of class Digits.
     */
    @Test
    public void testConvertINTtoBASE_int_int() {
        System.out.println("convertINTtoBASE");
        int input = 10;
        int outputRadix = 16;
        String expResult = "A";
        String result = Digits.convertINTtoBASE(input, outputRadix);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertINTtoBASE method, of class Digits.
     */
    @Test
    public void testConvertINTtoBASE_3args() {
        System.out.println("convertINTtoBASE");
        int input = 3;
        int outputRadix = 2;
        int padding = 8;
        String expResult = "00000011";
        String result = Digits.convertINTtoBASE(input, outputRadix, padding);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDOUBLEtoBASE method, of class Digits.
     */
    @Test
    public void testConvertDOUBLEtoBASE_double_int() {
        System.out.println("convertDOUBLEtoBASE");
        double input = 1.5;
        int outputRadix = 2;
        String expResult = "1.1";
        String result = Digits.convertDOUBLEtoBASE(input, outputRadix);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertDOUBLEtoBASE method, of class Digits.
     */
    @Test
    public void testConvertDOUBLEtoBASE_3args() {
        System.out.println("convertDOUBLEtoBASE");
        double input = 1.5;
        int outputRadix = 2;
        int padding = 4;
        String expResult = "0001.1";
        String result = Digits.convertDOUBLEtoBASE(input, outputRadix, padding);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertLONGtoBASE method, of class Digits.
     */
    @Test
    public void testConvertLONGtoBASE_long_int() {
        System.out.println("convertLONGtoBASE");
        long input = 100032;
        int outputRadix = 16;
        String expResult = "186C0";
        String result = Digits.convertLONGtoBASE(input, outputRadix);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertLONGtoBASE method, of class Digits.
     */
    @Test
    public void testConvertLONGtoBASE_3args() {
        System.out.println("convertLONGtoBASE");
        long input = 2030;
        int outputRadix = 16;
        int padding = 8;
        String expResult = "000007EE";
        String result = Digits.convertLONGtoBASE(input, outputRadix, padding);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertBASEtoINT method, of class Digits.
     */
    @Test
    public void testConvertBASEtoINT() {
        System.out.println("convertBASEtoINT");
        String input = "10011";
        int inputRadix = 2;
        int expResult = 19;
        int result = Digits.convertBASEtoINT(input, inputRadix);
        assertEquals(expResult, result);
    }

    /**
     * Test of convertBASEtoDOUBLE method, of class Digits.
     */
    @Test
    public void testConvertBASEtoDOUBLE() {
        System.out.println("convertBASEtoDOUBLE");
        String input = "10.1";
        int inputRadix = 2;
        double expResult = 2.5;
        double result = Digits.convertBASEtoDOUBLE(input, inputRadix);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of convertBASEtoLONG method, of class Digits.
     */
    @Test
    public void testConvertBASEtoLONG() {
        System.out.println("convertBASEtoLONG");
        String input = "ABCDEF";
        int inputRadix = 16;
        long expResult = 11259375L;
        long result = Digits.convertBASEtoLONG(input, inputRadix);
        assertEquals(expResult, result);
    }

    /**
     * Test of separateNumString method, of class Digits.
     */
    @Test
    public void testSeparateNumString() {
        System.out.println("separateNumString");
        String input = "123+123=246";
        String[] expResult = {"123","+","123","=","246"};
        String[] result = Digits.separateNumString(input);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of greatestCommonDivisor method, of class Digits.
     */
    @Test
    public void testGreatestCommonDivisor_int_int() {
        System.out.println("greatestCommonDivisor");
        int a = 100;
        int b = 12;
        int expResult = 4;
        int result = Digits.greatestCommonDivisor(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of greatestCommonDivisor method, of class Digits.
     */
    @Test
    public void testGreatestCommonDivisor_long_long() {
        System.out.println("greatestCommonDivisor");
        long a = 1000000L;
        long b = 750L;
        long expResult = 250L;
        long result = Digits.greatestCommonDivisor(a, b);
        assertEquals(expResult, result);
    }

    /**
     * Test of extendedEuclideanAlgorithm method, of class Digits.
     */
    @Test
    public void testExtendedEuclideanAlgorithm_int_int() {
        System.out.println("extendedEuclideanAlgorithm");
        int a = 180;
        int b = 150;
        int[] expResult = {30,1,-1};        
        int[] result = Digits.extendedEuclideanAlgorithm(a, b);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of extendedEuclideanAlgorithm method, of class Digits.
     */
    @Test
    public void testExtendedEuclideanAlgorithm_long_long() {
        System.out.println("extendedEuclideanAlgorithm");
        long a = 2000L;
        long b = 25000L;
        long[] expResult = {1000,1,-12};
        long[] result = Digits.extendedEuclideanAlgorithm(a, b);
        assertArrayEquals(expResult, result);
    }
    
}
