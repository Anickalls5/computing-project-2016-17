/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectPackage.MODEL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam
 */
public class BitwiseTest {
    
    public BitwiseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of NOT method, of class Bitwise.
     */
    @Test
    public void testNOT() {
        System.out.println("NOT");
        String input = "100001101";
        String expResult = "011110010";
        String result = Bitwise.NOT(input);
        assertEquals(expResult, result);
    }

    /**
     * Test of AND method, of class Bitwise.
     */
    @Test
    public void testAND() {
        System.out.println("AND");
        String input1 = "10010111";
        String input2 = "00011110";
        String expResult = "00010110";
        String result = Bitwise.AND(input1, input2);
        assertEquals(expResult, result);
    }

    /**
     * Test of OR method, of class Bitwise.
     */
    @Test
    public void testOR() {
        System.out.println("OR");
        String input1 = "0111110101";
        String input2 = "0010101011";
        String expResult = "0111111111";
        String result = Bitwise.OR(input1, input2);
        assertEquals(expResult, result);
    }

    /**
     * Test of XOR method, of class Bitwise.
     */
    @Test
    public void testXOR() {
        System.out.println("XOR");
        String input1 = "1010010111";
        String input2 = "0011101010";
        String expResult = "1001111101";
        String result = Bitwise.XOR(input1, input2);
        assertEquals(expResult, result);
    }
    
}
