/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectPackage.CONTROLLER;

import ProjectPackage.MODEL.Message;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam
 */
public class MonoalphabeticTest {
    
    public MonoalphabeticTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt method, of class Monoalphabetic.
     */
    @Test
    public void testEncrypt() {
        System.out.println("MONOALPHABETIC encrypt");
        Message input = new Message("hello");
        Monoalphabetic instance = new Monoalphabetic();
        instance.setShiftValue(5);
        Message expResult = new Message("mjqqt");
        Message result = instance.encrypt(input);
        assertEquals(expResult.getText(), result.getText());
    }

    /**
     * Test of decrypt method, of class Monoalphabetic.
     */
    @Test
    public void testDecrypt() {
        System.out.println("MONOALPHABETIC decrypt");
        Message input = new Message("vwxyz");
        Monoalphabetic instance = new Monoalphabetic();
        instance.setShiftValue(-5);
        Message expResult = new Message("abcde");
        Message result = instance.decrypt(input);
        assertEquals(expResult.getText(), result.getText());
    }

    /**
     * Test of shiftCharacter method, of class Monoalphabetic.
     */
    @Test
    public void testShiftCharacter() {
        System.out.println("shiftCharacter");
        char inputCharacter = 'X';
        int shiftValue = 25;
        Monoalphabetic instance = new Monoalphabetic();
        char expResult = 'W';
        char result = instance.shiftCharacter(inputCharacter, shiftValue);
        assertEquals(expResult, result);
    }
    
}
