/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectPackage.CONTROLLER;

import ProjectPackage.MODEL.Message;
import java.math.BigInteger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam
 */
public class RSATest {
    
    public RSATest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt method, of class RSA.
     */
    @Test
    public void testEncrypt() {
        System.out.println("RSA encrypt");
        
        int[] asciis = {104,101,108,108,111};
        Message input = new Message(asciis);
        
        BigInteger p = new BigInteger("9857");
        BigInteger q = new BigInteger("6067");
        BigInteger n = new BigInteger("59802419");
        BigInteger e = new BigInteger("9594941");
        BigInteger d = new BigInteger("20639765");
        
        BigInteger[] bigInAsciis = {p,q,n,e,d};                        
        RSA instance = new RSA(bigInAsciis);
        
        
        BigInteger[] bigOutAsciis = {new BigInteger("10987526"), new BigInteger("9348339"), new BigInteger("32897577"), new BigInteger("32897577"), new BigInteger("23311278")};
        Message expResult = new Message(bigOutAsciis);
        
        
        Message result = instance.encrypt(input);
        assertEquals(expResult.getText(), result.getText());
    }

    /**
     * Test of decrypt method, of class RSA.
     */
    @Test
    public void testDecrypt() {
        System.out.println("RSA decrypt");
        
        int[] asciis = {32875,38207,280134,202959,269415};
        Message input = new Message(asciis);
        
        BigInteger p = new BigInteger("509");
        BigInteger q = new BigInteger("577");
        BigInteger n = new BigInteger("293693");
        BigInteger e = new BigInteger("24059");
        BigInteger d = new BigInteger("265523");
        
        BigInteger[] bigInAsciis = {p,q,n,e,d};                        
        RSA instance = new RSA(bigInAsciis);
               
        Message expResult = new Message("world");
        
        Message result = instance.decrypt(input);
        assertEquals(expResult.getText(), result.getText());
    }
    
}
