/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectPackage.CONTROLLER;

import ProjectPackage.MODEL.Message;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam
 */
public class PolyalphabeticTest {
    
    public PolyalphabeticTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt method, of class Polyalphabetic.
     */
    @Test
    public void testEncrypt() {
        System.out.println("POLYALPHABETIC encrypt");
        Message input = new Message("hello");
        Polyalphabetic instance = new Polyalphabetic();
        instance.setPassword("brown");
        Message expResult = new Message("ivzhb");
        Message result = instance.encrypt(input);
        assertEquals(expResult.getText(), result.getText());
    }
   

    /**
     * Test of decrypt method, of class Polyalphabetic.
     */
    @Test
    public void testDecrypt() {
        System.out.println("POLYALPHABETIC decrypt");
        Message input = new Message("xzlpe");
        Polyalphabetic instance = new Polyalphabetic();
        instance.setPassword("blue");
        Message expResult = new Message("world");
        Message result = instance.decrypt(input);
        assertEquals(expResult.getText(), result.getText());
    }
    
}
