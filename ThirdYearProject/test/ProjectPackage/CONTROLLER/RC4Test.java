/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProjectPackage.CONTROLLER;

import ProjectPackage.MODEL.Message;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adam
 */
public class RC4Test {
    
    public RC4Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of symmetricEncryption method, of class RC4.
     */
    @Test
    public void testSymmetricEncryption1() {
        System.out.println("RC4 encrypt");
        
        int[] inAsciis = {104,101,108,108,111};
        Message input = new Message(inAsciis);
        boolean encryption = true;
        RC4 instance = new RC4("green");
        
        int[] outAsciis = {30,181,108,100,112};
        
        Message expResult = new Message(outAsciis);
        Message result = instance.symmetricEncryption(input, encryption);
        assertEquals(expResult.getText(), result.getText());
    }

    /**
     * Second test of symmetricEncryption method, of class RC4.
     */
    @Test
    public void testSymmetricEncryption2() {
        System.out.println("RC4 decrypt");
        
        int[] inAsciis = {218,174,111,155,185};
        Message input = new Message(inAsciis);
        boolean encryption = false;
        RC4 instance = new RC4("pink");
        
        int[] outAsciis = {119,111,114,108,100};
        
        Message expResult = new Message(outAsciis);
        Message result = instance.symmetricEncryption(input, encryption);
        assertEquals(expResult.getText(), result.getText());
    }

    
}
