package ProjectPackage.MODEL;
import java.math.BigInteger;

/** This class is to represent a message which holds various representations of
 *  text which can be encrypt or decrypt using various ciphers.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public class Message {
    
// FIELDS    
    
    private int[] codes;
    private String text;
    private BigInteger[] bigCodes;
    
// CONSTRUCTORS    
    
    public Message(){}
    
    public Message(String textInput){
                
        text = textInput;        
        char[] values = textInput.toCharArray();
        codes = new int[values.length];
        bigCodes = new BigInteger[values.length];
        
        for(int i = 0; i < values.length; i++){
            codes[i] = (int)values[i];
            bigCodes[i] = new BigInteger(""+(int)values[i]);
        }
        
    }
    
    public Message(int[] codeInput){
        codes = codeInput;
        bigCodes = new BigInteger[codes.length];
        StringBuilder S = new StringBuilder();
        
        for(int current : codeInput){
            S.append((char)current);
        }
        
        text = S.toString();
        
        for(int i = 0; i < codes.length; i++){
            bigCodes[i] = new BigInteger(""+codes[i]);
        }
    }
    
    public Message(long[] codeInput){        
        codes = new int[codeInput.length];
        bigCodes = new BigInteger[codeInput.length];
        StringBuilder S = new StringBuilder();        
        
        for(int i = 0; i < codeInput.length; i++){
            codes[i] = (int)codeInput[i];
            bigCodes[i] = new BigInteger(""+codeInput[i]);
        }        
        
        for(long current : codeInput){
            S.append((char)current);
        }
        
        text = S.toString();
    }
    
    public Message(BigInteger[] bigCodeInput){
        bigCodes = bigCodeInput;    
        codes = new int[bigCodes.length];        
        StringBuilder S = new StringBuilder();
        
        for(BigInteger current : bigCodeInput){
            S.append((char)current.longValue());
        }        

        text = S.toString();                        
        
        for(int i = 0; i < bigCodes.length; i++){
            codes[i] = bigCodes[i].intValue();
        }        
    }
    
// METHODS    
    
/** This function returns the codes integer array of the invoking object.
 * 
 * @return - [int[]] The array of ASCII codes of the object
 */    
    public int[] getCodes(){
        return codes;
    }
    
/** This function returns the codes array, but converts the values to longs.
 * 
 * @return - [long[]] An array of long values representing the ASCII codes
 */    
    public long[] getLongCodes(){
        
        long[] output = new long[codes.length];
        
        for(int i = 0; i < codes.length; i++){
            output[i] = codes[i];
        }
        
        return output;
    }
    
/** This function returns the bigCodes array of the invoking object.
 * 
 * @return - [BigInteger[]] An array of larger values (not necessarily ASCII codes)
 */    
    public BigInteger[] getBigCodes(){
        return bigCodes;
    }

/** This function returns the String representation of the invoking object.
 * 
 * @return - [String] The text representation of the invoking object
 */    
    public String getText(){
        return text;
    }

/** This function returns the ASCII code of the character located at the given index.
 * 
 * @param index - [int] The position in the Message to search at
 * @return - [int] The ASCII code of the character at this position
 */    
    public int getCodeAt(int index){        
        if(index >= this.codes.length || index < 0){
            return 0;
        }
        else{
            return codes[index];
        }
    }
    
/** This function returns the BigInteger representation of the character
 *  located at the given index.
 * 
 * @param index - [int] The position in the Message to search at
 * @return - [BigInteger] The BigInteger value of the character at this position
 */    
    public BigInteger getBigCodeAt(int index){        
        if(index >= bigCodes.length || index < 0){
            return new BigInteger("0");
        }
        else{
            return bigCodes[index];
        }
    }

/** This function returns the character located at the given index.
 * 
 * @param index - [int] The position in the Message to search at
 * @return - [char] The character located at this position
 */    
    public char getCharAt(int index){
        
        if(index >= this.codes.length || index < 0){
            return ' ';
        }
        else{
            return (char)codes[index];
        }
    }
    
/** This function assigns the invoking object's codes field to the given integer
 *  array.
 * 
 * @param input - [int[]] The new array of ASCII codes
 */    
    public void setCodes(int[] input){
        this.codes = input;
        this.bigCodes = new BigInteger[input.length];
        
        StringBuilder S = new StringBuilder();
        for(int i = 0; i < input.length; i++){
            S.append((char)input[i]);
            bigCodes[i] = new BigInteger(""+input[i]);
        }
        this.text = S.toString();
    }
    
/** This function assigns the invoking object's bigCodes field to the given
 *  array of BigInteger values.
 * 
 * @param input - [BigInteger] The new BigInteger values
 */    
    public void setBigCodes(BigInteger[] input){
        this.bigCodes = input;
        
        StringBuilder S = new StringBuilder();
        for(BigInteger current : bigCodes){
            S.append((char)current.longValue());
        }
        this.text = S.toString();       
    }
    
/** This function assigns the invoking object's text field to the given String. 
 * 
 * @param input - [String] The new text String of the Message
 */    
    public void setText(String input){
        this.text = input;
        
        char[] inputArray = input.toCharArray();
        this.codes = new int[inputArray.length];
        this.bigCodes = new BigInteger[inputArray.length];
        
        for(int i = 0; i < inputArray.length; i++){
            this.codes[i] = (int)inputArray[i];
            this.bigCodes[i] = new BigInteger(""+(int)inputArray[i]);
        }    
    }

/** This function converts all characters in the Message to their uppercase 
 *  equivalents.
 */    
    public void uppercase(){        
        this.text = this.text.toUpperCase();
    }

/** This function converts all characters in the Message to their lowercase
 *  equivalents.
 */    
    public void lowercase(){
        setText(getText().toLowerCase());
    }
    

/** This function prints the ASCII codes of each of the characters in the Message 
 *  to the console.
 */    
    public void printCodes(){
        for(int current : codes){
            System.out.print(current+" ");
        }
        System.out.println("");
    }

/** This function prints the BigInteger values of each of the characters in the
 *  Message to the console. 
 */    
    public void printBigCodes(){
        for(BigInteger current : bigCodes){
            System.out.print(current.longValue()+" ");
        }
        System.out.println("");
    }

/** This function prints each of the characters in the Message to the console. 
 */    
    public void printText(){
        char[] textArray = this.text.toCharArray();
        for(char current : textArray){
            System.out.print(current+" ");
        }
        System.out.println("");
    }        
    
}