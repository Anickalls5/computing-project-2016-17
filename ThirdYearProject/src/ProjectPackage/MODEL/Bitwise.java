package ProjectPackage.MODEL;

/** This abstract class is a self-written class that contains methods to perform
 *  four logical operations on string representations of binary numbers. Only
 *  the four basic logical operators are implemented here, but additional 
 *  operations can be added in the future if necessary.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public abstract class Bitwise {
    
    
/** This function is an implementation of the NOT (¬) logical operation, it takes
 *  a binary string and returns another: the output of the not operation.
 * 
 *  @param input - [String] The binary number, represented as a string
 *  @return - [String] A binary string representing the NOT of the input
 */    
    public static String NOT(String input){
        char[] inputArray = input.toCharArray();
        StringBuilder output = new StringBuilder();
        
        // FIRST WE INITIALISE THE OUTPUT AND SPLIT THE INPUT INTO A CHARACTER
        // ARRAY
        
        for(int i = 0; i < inputArray.length; i++){
            char value = inputArray[i];
            switch(value){
                case '0' : output.append("1"); break;
                case '1' : output.append("0"); break;
                default : throw new IllegalArgumentException("PLEASE ENSURE THAT THE INPUT IS A BINARY NUMBER"); 
            }            
        }        
        // FOR EACH OF THE CHARACTERS, WE CHECK IF IT IS A ONE OR ZERO, AND THEN
        // SWAP THEM DEPENDING ON THEIR VALUE (NEGATE THEM). IF ANY OF THE 
        // CHARACTERS ARE NEITHER A ONE OR ZERO, THEN AN EXCEPTION IS THROWN
        
        return output.toString();
    }
    
    
/** This function is an implementation of the AND (&amp;&amp;) logical operation, it takes
 *  two binary strings and returns a third: the output of the and operation.
 * 
 *  @param input1 - [String] The first binary string operand 
 *  @param input2 - [String] The second binary string operand
 *  @return - [String] A binary string representing the AND of the two inputs
 */    
    public static String AND(String input1, String input2){
        StringBuilder output = new StringBuilder();
        char[] input1Array = input1.toCharArray();
        char[] input2Array = input2.toCharArray();
        // THE OUTPUT IS INITIALISED AND THE INPUTS ARE SPLIT INTO CHARACTER ARRAYS
        
        if(input1Array.length != input2Array.length){
            throw new IllegalArgumentException("PLEASE ENSURE THAT THE TWO BINARY INPUTS ARE OF THE SAME LENGTH.");   
        }
        // THE BINARY NUMBERS MUST BE OF AN EQUAL LENGTH, IF THEY ARE NOT, AN
        // EXCEPTION IS THROWN
        
        for(int i = 0; i < input1Array.length; i++){
            int value1 = (int)input1Array[i] - 48;
            int value2 = (int)input2Array[i] - 48;
            
            if(value1 < 0 || value1 > 1 || value2 < 0 || value2 > 1){
                throw new IllegalArgumentException("PLEASE ENSURE THAT THE TWO INPUTS ARE VALID BINARY NUMBERS");
            }
            
            if(value1 == 1 && value2 == 1){
                output.append("1");
            }
            else{
                output.append("0"); 
            }
        }        
        // FOR ALL CHARACTERS IN THE PAIR OF INPUTS, IF BOTH OF THE CURRENT CHARACTERS
        // ARE 1, THEN WE APPEND A 1 TO THE OUTPUT, OTHERWISE, WE APPEND A 0.
        
        return output.toString();
    }
    
    
/** This function is an implementation of the OR (||) logical operation, it takes
 *  two binary strings and returns a third: the output of the or operation.
 * 
 *  @param input1 - [String] The first binary string operand
 *  @param input2 - [String] The second binary string operand
 *  @return - [String] A binary string representing the OR of the two inputs
 */    
    public static String OR(String input1, String input2){
        StringBuilder output = new StringBuilder();
        char[] input1Array = input1.toCharArray();
        char[] input2Array = input2.toCharArray();
        // THE OUTPUT IS INITIALISED AND THE INPUTS SPLIT INTO CHARACTER ARRAYS
        
        if(input1Array.length != input2Array.length){
            throw new IllegalArgumentException("PLEASE ENSURE THAT THE TWO BINARY INPUTS ARE OF THE SAME LENGTH.");   
        }
        // AGAIN, THE INPUTS MUST BE OF THE SAME LENGTH
        
        for(int i = 0; i < input1Array.length; i++){
            int value1 = (int)input1Array[i] - 48;
            int value2 = (int)input2Array[i] - 48;
            
            if(value1 < 0 || value1 > 1 || value2 < 0 || value2 > 1){
                throw new IllegalArgumentException("PLEASE ENSURE THAT THE TWO INPUTS ARE VALID BINARY NUMBERS");
            }
            
            if(value1 == 1 || value2 == 1){
                output.append("1");
            }
            else{
                output.append("0"); 
            }
        }        
        // FOR EACH OF THE CHARACTERS, IF AT LEAST ONE OF THE CURRENT CHARACTERS
        // IS A 1, WE APPEND A 1 TO THE OUTPUT, OTHERWISE, WE APPEND A ZERO
        
        return output.toString();        
    }
    
    
/** This function is an implementation of the XOR (⊕) logical operation, it takes
 *  two binary strings and returns a third: the output of the XOR operation.
 * 
 *  @param input1 - [String] The first binary string operand
 *  @param input2 - [String] The second binary string operand
 *  @return - [String] A binary string representing the XOR of the two inputs
 */    
    public static String XOR(String input1, String input2){
        StringBuilder output = new StringBuilder();
        char[] input1Array = input1.toCharArray();
        char[] input2Array = input2.toCharArray();
        // THE OUTPUT IS INITIALISED AND THE INPUTS SPLIT INTO CHARACTER ARRAYS
        
        if(input1Array.length != input2Array.length){
            throw new IllegalArgumentException("PLEASE ENSURE THAT THE TWO BINARY INPUTS ARE OF THE SAME LENGTH.");   
        }
        
        for(int i = 0; i < input1Array.length; i++){
            int value1 = (int)input1Array[i] - 48;
            int value2 = (int)input2Array[i] - 48;
            
            if(value1 < 0 || value1 > 1 || value2 < 0 || value2 > 1){
                throw new IllegalArgumentException("PLEASE ENSURE THAT THE TWO INPUTS ARE VALID BINARY NUMBERS");
            }
            
            if((value1 == 1 && value2 == 0) || (value1 == 0 && value2 == 1)){
                output.append("1");
            }
            else{
                output.append("0"); 
            }
        }        
        // FOR EACH OF THE CHARACTERS, IF EXACTLY ONE OF THE CURRENT CHARACTERS
        // IS A 1, WE APPEND A 1 TO THE OUTPUT, OR A 0 OTHERWISE
        
        return output.toString();    
    }
    

}