package ProjectPackage.MODEL;
import java.math.BigDecimal;


/** This abstract class contains self-written methods for various manipulations
 *  of numbers used in the project. Again, this is not a finite list, and more
 *  functions can be added if necessary in the future.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public abstract class Digits {
        
    
/** This function converts a given integer into a String and returns it. 
 * 
 * @param input - [int] The integer to convert
 * @return - [String] The String representation of the given integer
 */ 
    public static String convertINTtoSTRING(int input){
        StringBuilder output = new StringBuilder();
        output.append(input);
        return output.toString();
    }
        
/** This function converts a given String into an integer and returns it.
 * 
 * @param input - [String] The String to convert
 * @return - [int] The integer value of the passed String
 */    
    public static int convertSTRINGtoINT(String input){        
        if(input.equals("")){
            return 0;
        }
        
        BigDecimal output = new BigDecimal(input);
        return output.intValue();
    }    
    
/** This function converts a given double into a String and returns it. 
 * 
 * @param input - [double] The integer to convert
 * @return - [String] The String representation of the given double
 */     
    public static String convertDOUBLEtoSTRING(double input){
        StringBuilder output = new StringBuilder();
        output.append(input);
        return output.toString();
    }    
    
/** This function converts a given String into a double and returns it.
 * 
 * @param input - [String] The String to convert
 * @return - [double] The double value of the passed String
 */       
    public static double convertSTRINGtoDOUBLE(String input){        
        if(input.equals("")){
            return 0;
        }
        
        BigDecimal output = new BigDecimal(input);
        return output.doubleValue();
    }      
        
/** This function converts a given long into a String and returns it. 
 * 
 * @param input - [long] The integer to convert
 * @return - [String] The String representation of the given integer
 */     
    public static String convertLONGtoSTRING(long input){
        StringBuilder output = new StringBuilder();
        output.append(input);
        return output.toString();        
    }    
    
/** This function converts a given String into a long and returns it.
 * 
 * @param input - [String] The String to convert
 * @return - [long] The long value of the passed String
 */     
    public static long convertSTRINGtoLONG(String input){        
        if(input.equals("")){
            return 0;
        }
        
        BigDecimal output = new BigDecimal(input);
        return output.longValue();        
    }
    


/** This function returns the number of digits in the passed integer value.
 * 
 * @param input - [int] The integer to count the digits of
 * @return - [int] The number of digits in the passed integer 
 */    
    public static int countDigits(int input){
        String X = Digits.convertINTtoSTRING(input); 
        return X.length();
    }

/** This function returns the number of digits in the passed double value.
 * 
 * @param input - [double] The double to count the digits of
 * @return - [int] The number of digits in the passed double 
 */       
    public static int countDigits(double input){
        String X = Digits.convertDOUBLEtoSTRING(input);
        char[] xArray = X.toCharArray();
        int count = xArray.length;
        
        for(char current : xArray){
            int ascii = (int)current - 48;
            
            if(ascii < 0 || ascii > 9){
                count--;
            }
        }
        return count;
    }
    
/** This function returns the number of digits in the passed String.
 * 
 * @param input - [String] The String to count the digits of
 * @return - [int] The number of digits in the passed String 
 */       
    public static int countDigits(String input){
        char[] inputArray = input.toCharArray();
        int count = inputArray.length;
        
        for(char current : inputArray){
            int ascii = (int)current - 48;
            if(ascii < 0 || ascii > 9){
                count--;
            }            
        }
        
        return count;
    }
    
    
    
/** This function returns a boolean value indicating whether the passed integer
 *  is a prime number or not.
 * 
 * @param input - [int] The integer to check
 * @return - [boolean] An indication as to whether the passed integer is prime
 */    
    public static boolean isPrime(int input){
        double value = (double)input;
        
        if(value == 1.0){
            return true;
        }
        
        double root = Math.sqrt(value);
        for(double i = 2.0; i <= root; i += 1){
            double remainder = value % i;
            
            if(remainder == 0){
                return false;
            }
        }
        return true;
    }


                
/** This function returns a boolean value indicating whether the passed long
 *  is a prime number or not.
 * 
 * @param input - [long] The long to check
 * @return - [boolean] An indication as to whether the passed long is prime
 */        
    public static boolean isPrime(long input){
        double value = (double)input;
        
        if(value == 1.0){
            return true;
        }
        
        double root = Math.sqrt(value);
        for(double i = 2.0; i <= root; i += 1){
            double remainder = value % i;
            
            if(remainder == 0){
                return false;
            }
        }
        return true;
    }
    
    
    
/** This function splits the passed double value into an array of Strings: one 
 *  represents the integer part of the decimal, the other represents the 
 *  fractional part. The integer part of the decimal is located at index 0.
 * 
 * @param input - [double] The decimal to split
 * @return [String[]] An array of Strings containing the integer and fractional parts
 */    
    public static String[] splitDecimal(double input){
        String[] output = new String[2];
        StringBuilder integerPart = new StringBuilder();
        StringBuilder fractionPart = new StringBuilder();
        
        StringBuilder stream = new StringBuilder();
        stream.append(input);
        
        char[] inputArray = stream.toString().toCharArray();
        boolean pointReached = false;
        
        for(char current : inputArray){
            if(current == '.'){
                pointReached = true;
                continue;
            }
            
            if(pointReached == true){
                fractionPart.append(current);
            }
            else{
                integerPart.append(current);
            }
        }
        
        output[0] = integerPart.toString();
        output[1] = fractionPart.toString();
        return output;
    }
    
/** This function splits the passed String into an array of Strings: one 
 *  represents the integer part of the decimal String, the other represents the 
 *  fractional part. The integer part of the decimal String is located at index 0.
 * 
 * @param input - [String] The decimal String to split
 * @return [String[]] An array of Strings containing the integer and fractional parts
 */     
    public static String[] splitDecimal(String input){
        String[] output = new String[2];
        StringBuilder integerPart = new StringBuilder();
        StringBuilder fractionPart = new StringBuilder();
        
        char[] inputArray = input.toCharArray();
        boolean pointReached = false;
        
        for(char current : inputArray){
            if(current == '.'){
                pointReached = true;
                continue;
            }
            
            if(pointReached == true){
                fractionPart.append(current);
            }
            else{
                integerPart.append(current);
            }
        }
        
        output[0] = integerPart.toString();
        output[1] = fractionPart.toString();
        return output;
    }
    
/** This function 'chops' the passed double value to the given accuracy (decimal
 *  places) and returns it as a String WITHOUT rounding of any kind.
 * 
 * @param input - [double] The decimal to trim
 * @param accuracy - [int] The number of decimal places to trim the decimal to
 * @return - [String] The trimmed decimal String
 */            
    public static String trimDecimal(double input, int accuracy){
        
        String[] parts = splitDecimal(input);
        String fractionPart = parts[1];
        
        // PAD THE NUMBER IF NECESSARY
        
        if(accuracy == 0){
            return parts[0];
        }
        
        if(accuracy > fractionPart.length()){
            do{
                fractionPart = fractionPart+"0";
            }while(accuracy > fractionPart.length());
        }
        
        String output = "";
        
        for(int i = 0; i < accuracy; i++){
            output = output+fractionPart.charAt(i);
        }
        
        return parts[0]+"."+output;        
    }

/** This function 'chops' the passed String to the given accuracy (decimal
 *  places) and returns it as a String WITHOUT rounding of any kind.
 * 
 * @param input - [String] The decimal String to trim
 * @param accuracy - [int] The number of decimal places to trim the decimal to
 * @return - [String] The trimmed decimal String
 */      
    public static String trimDecimal(String input, int accuracy){
        String integerPart = "";
        String fractionPart = "";
        boolean pointReached = false;
               
        for(int i = 0; i < input.length(); i++){
            if(input.charAt(i) == '.'){
                pointReached = true;
                continue;
            }
            
            if(pointReached){
                fractionPart = fractionPart+input.charAt(i);
                continue;
            }         
            
            integerPart = integerPart+input.charAt(i);            
        }
        
        if(accuracy == 0){
            return integerPart;
        }
        
        // PAD THE NUMBER IF NECESSARY
        
        if(accuracy > fractionPart.length()){
            do{
                fractionPart = fractionPart+"0";
            }while(accuracy > fractionPart.length());
        }
        
        String output = "";
        
        for(int i = 0; i < accuracy; i++){
            output = output+fractionPart.charAt(i);
        }       
        
        return integerPart+"."+output;        
    }
    
/** This function rounds the passed double value to the given accuracy (decimal
 *  places) and returns it as a String WITH rounding up.
 * 
 * @param input - [double] The decimal value to round
 * @param accuracy - [int] The number of decimal places to round the decimal to
 * @return - [String] The rounded decimal String
 */    
    public static String roundDecimal(double input, int accuracy){
     
        String[] parts = splitDecimal(input); 
        String fractionPart = parts[1];
        StringBuilder output = new StringBuilder();
        
        if(fractionPart.length() == accuracy){
            return ""+input;
        }                
        
        String trimmed;
        
        trimmed = fractionPart.substring(0, accuracy);
        
        int integerPart = convertSTRINGtoINT(parts[0]);
        int originalLength = trimmed.length();
        int trimmedValue = convertSTRINGtoINT(trimmed);
        char round = fractionPart.charAt(accuracy);        
        
        if(accuracy == 0){
            return ""+integerPart;
        }
        
        String newTrimmedValue;            
        
        if(((int)round-48) >= 5){
            trimmedValue += 1;

            String S = ""+trimmedValue;
            int newLength = S.length();
            
            if(newLength == originalLength+1){
                int addend = convertSTRINGtoINT(""+S.charAt(0));
                newTrimmedValue = S.substring(1,S.length());
                integerPart += addend;
            }
            else{
                newTrimmedValue = S;
            }
            
        }
        else{
            newTrimmedValue = ""+trimmedValue;
        }
        
        output.append(integerPart+"."+newTrimmedValue);
        
        return output.toString();
       
    }        
    
/** This function rounds the passed String to the given accuracy (decimal
 *  places) and returns it as a String WITH rounding up.
 * 
 * @param input - [String] The decimal to round
 * @param accuracy - [int] The number of decimal places to round the decimal to
 * @return - [String] The rounded decimal String
 */       
    public static String roundDecimal(String input, int accuracy){
     
        String[] parts = splitDecimal(input); 
        String fractionPart = parts[1];
        StringBuilder output = new StringBuilder();
        
        if(fractionPart.length() == accuracy){
            return ""+input;
        }                
        
        String trimmed;
        
        trimmed = fractionPart.substring(0, accuracy);
        
        int integerPart = convertSTRINGtoINT(parts[0]);
        int originalLength = trimmed.length();
        int trimmedValue = convertSTRINGtoINT(trimmed);
        char round = fractionPart.charAt(accuracy);        
        
        if(accuracy == 0){
            return ""+integerPart;
        }
        
        String newTrimmedValue;            
        
        if(((int)round-48) >= 5){
            trimmedValue += 1;

            String S = ""+trimmedValue;
            int newLength = S.length();
            
            if(newLength == originalLength+1){
                int addend = convertSTRINGtoINT(""+S.charAt(0));
                newTrimmedValue = S.substring(1,S.length());
                integerPart += addend;
            }
            else{
                newTrimmedValue = S;
            }
            
        }
        else{
            newTrimmedValue = ""+trimmedValue;
        }
        
        output.append(integerPart+"."+newTrimmedValue);
        
        return output.toString();
       
    }    

   
    
/** This function converts the given integer to the equivalent value in the number
 *  system that has the radix of the passed integer and returns it as a String.
 * 
 * @param input - [int] The integer to convert
 * @param outputRadix - [int] The radix of the number system to convert to
 * @return - [String] The converted integer
 */            
    public static String convertINTtoBASE(int input, int outputRadix){            
        StringBuilder output = new StringBuilder();
        
        int remainder;
        int newValue;
        int value = input;
        
        do{
            newValue = value / outputRadix;
            remainder = value % outputRadix;

            if(outputRadix == 16){
                switch(remainder){
                    case 10 : output.insert(0,'A'); break;
                    case 11 : output.insert(0,'B'); break;
                    case 12 : output.insert(0,'C'); break;
                    case 13 : output.insert(0,'D'); break;
                    case 14 : output.insert(0,'E'); break;
                    case 15 : output.insert(0,'F'); break;
                    default : output.insert(0,remainder); break;
                }
                value = newValue;
            }
            else{            
                output.insert(0,remainder);                        
                value = newValue;
            }        
        }while(value > 0);
               
        return output.toString();
    }
    
/** This function converts the given integer to the equivalent value in the number
 *  system that has the radix of the passed integer and returns it as a String,
 *  but with the given number of digits (padding).
 * 
 * @param input - [int] The integer to convert
 * @param outputRadix - [int] The radix of the number system to convert to
 * @param padding - [int] The desired length of the converted number (digits)
 * @return - [String] The converted integer
 */                
    public static String convertINTtoBASE(int input, int outputRadix, int padding){            
        StringBuilder output = new StringBuilder();
        
        int remainder;
        int newValue;
        int value = input;
        
        for(int i = 0; i < padding; i++){
            newValue = value / outputRadix;
            remainder = value % outputRadix;

            if(outputRadix == 16){
                switch(remainder){
                    case 10 : output.insert(0,'A'); break;
                    case 11 : output.insert(0,'B'); break;
                    case 12 : output.insert(0,'C'); break;
                    case 13 : output.insert(0,'D'); break;
                    case 14 : output.insert(0,'E'); break;
                    case 15 : output.insert(0,'F'); break;
                    default : output.insert(0,remainder); break;
                }
                value = newValue;
            }
            else{            
                output.insert(0,remainder);                        
                value = newValue;
            }        
        }
               
        return output.toString();
    }
        
/** This function converts the given double to the equivalent value in the number
 *  system that has the radix of the passed integer and returns it as a String.
 * 
 * @param input - [double] The double to convert
 * @param outputRadix - [int] The radix of the number system to convert to
 * @return - [String] The converted double 
 */       
    public static String convertDOUBLEtoBASE(double input, int outputRadix){        
        String[] inputArray = splitDecimal(input);        
        int integerPart = convertSTRINGtoINT(inputArray[0]);       
        StringBuilder output = new StringBuilder();
        
        int remainder;
        int newValue;
        int value = integerPart;
        
        do{
            newValue = value / outputRadix;
            remainder = value % outputRadix;

            if(outputRadix == 16){
                switch(remainder){
                    case 10 : output.insert(0,'A'); break;
                    case 11 : output.insert(0,'B'); break;
                    case 12 : output.insert(0,'C'); break;
                    case 13 : output.insert(0,'D'); break;
                    case 14 : output.insert(0,'E'); break;
                    case 15 : output.insert(0,'F'); break;
                    default : output.insert(0,remainder); break;
                }
                value = newValue;
            }
            else{            
                output.insert(0,remainder);                        
                value = newValue;
            }        
        }while(value > 0);
        
        output.append(".");
        
        String valueString = "0."+inputArray[1];
        double fractionValue = convertSTRINGtoDOUBLE(valueString);        
        double newFractionValue;
        int count = 0;
        
        do{
            newFractionValue = fractionValue*outputRadix;
            String[] X = splitDecimal(newFractionValue);
            
            if(outputRadix == 16){
                switch(X[0]){
                    case "10" : output.append("A"); break; 
                    case "11" : output.append("B"); break; 
                    case "12" : output.append("C"); break; 
                    case "13" : output.append("D"); break; 
                    case "14" : output.append("E"); break; 
                    case "15" : output.append("F"); break; 
                    default : output.append(X[0]); break;
                }            
            }          
            else{            
                output.append(X[0]);
            }
            
            if(X[0].compareTo("0") != 0){
                fractionValue = convertSTRINGtoDOUBLE("0."+X[1]);
            }
            else{
                fractionValue = newFractionValue;
            }
            
            count++;
        }while(fractionValue > 0 && count <= 25);
        
        if(count > 15){
            //output.append("...");
        }
        
// A LIMIT OF 25 DECIMAL PLACES HAS BEEN IMPLEMENTED TO AVOID MEMORY ERRORS        
        return output.toString();
    }
    
/** This function converts the given double to the equivalent value in the number
 *  system that has the radix of the passed integer and returns it as a String,
 *  but with the given number of digits (padding).
 * 
 * @param input - [double] The double to convert
 * @param outputRadix - [int] The radix of the number system to convert to
 * @param padding - [int] The desired length of the converted number (digits)
 * @return - [String] The converted double
 */       
    public static String convertDOUBLEtoBASE(double input, int outputRadix, int padding){        
        String[] inputArray = splitDecimal(input);        
        int integerPart = convertSTRINGtoINT(inputArray[0]);       
        StringBuilder output = new StringBuilder();
        
        int remainder;
        int newValue;
        int value = integerPart;
        
        for(int i = 0; i < padding; i++){
            newValue = value / outputRadix;
            remainder = value % outputRadix;

            if(outputRadix == 16){
                switch(remainder){
                    case 10 : output.insert(0,'A'); break;
                    case 11 : output.insert(0,'B'); break;
                    case 12 : output.insert(0,'C'); break;
                    case 13 : output.insert(0,'D'); break;
                    case 14 : output.insert(0,'E'); break;
                    case 15 : output.insert(0,'F'); break;
                    default : output.insert(0,remainder); break;
                }
                value = newValue;
            }
            else{            
                output.insert(0,remainder);                        
                value = newValue;
            }        
        }
        
        output.append(".");
        
        String valueString = "0."+inputArray[1];
        double fractionValue = convertSTRINGtoDOUBLE(valueString);        
        double newFractionValue;
        int count = 0;
        
        do{
            newFractionValue = fractionValue*outputRadix;
            String[] X = splitDecimal(newFractionValue);
            
            if(outputRadix == 16){
                switch(X[0]){
                    case "10" : output.append("A"); break; 
                    case "11" : output.append("B"); break; 
                    case "12" : output.append("C"); break; 
                    case "13" : output.append("D"); break; 
                    case "14" : output.append("E"); break; 
                    case "15" : output.append("F"); break; 
                    default : output.append(X[0]); break;
                }            
            }          
            else{            
                output.append(X[0]);
            }
            
            if(X[0].compareTo("0") != 0){
                fractionValue = convertSTRINGtoDOUBLE("0."+X[1]);
            }
            else{
                fractionValue = newFractionValue;
            }
            
            count++;
        }while(fractionValue > 0 && count <= 25);
        
// A LIMIT OF 25 DECIMAL PLACES HAS BEEN IMPLEMENTED TO AVOID MEMORY ERRORS        
        return output.toString();
    }
    
/** This function converts the given long to the equivalent value in the number
 *  system that has the radix of the passed integer and returns it as a String.
 * 
 * @param input - [long] The long to convert
 * @param outputRadix - [int] The radix of the number system to convert to
 * @return - [String] The converted long 
 */       
    public static String convertLONGtoBASE(long input, int outputRadix){
        StringBuilder output = new StringBuilder();
        
        long remainder;
        long newValue;
        long value = input;
        
        do{
            newValue = value / outputRadix;
            remainder = value % outputRadix;

            if(outputRadix == 16){
                switch((int)remainder){
                    case 10 : output.insert(0,'A'); break;
                    case 11 : output.insert(0,'B'); break;
                    case 12 : output.insert(0,'C'); break;
                    case 13 : output.insert(0,'D'); break;
                    case 14 : output.insert(0,'E'); break;
                    case 15 : output.insert(0,'F'); break;
                    default : output.insert(0,remainder); break;
                }
                value = newValue;
            }
            else{            
                output.insert(0,remainder);                        
                value = newValue;
            }        
        }while(value > 0);
               
        return output.toString();        
    }
    
/** This function converts the given long to the equivalent value in the number
 *  system that has the radix of the passed integer and returns it as a String,
 *  but with the given number of digits (padding).
 * 
 * @param input - [long] The long to convert
 * @param outputRadix - [int] The radix of the number system to convert to
 * @param padding - [int] The desired length of the converted number (digits)
 * @return - [String] The converted long
 */       
    public static String convertLONGtoBASE(long input, int outputRadix, int padding){
        StringBuilder output = new StringBuilder();
        
        long remainder;
        long newValue;
        long value = input;
        
        for(int i = 0; i < padding; i++){
            newValue = value / outputRadix;
            remainder = value % outputRadix;

            if(outputRadix == 16){
                switch((int)remainder){
                    case 10 : output.insert(0,'A'); break;
                    case 11 : output.insert(0,'B'); break;
                    case 12 : output.insert(0,'C'); break;
                    case 13 : output.insert(0,'D'); break;
                    case 14 : output.insert(0,'E'); break;
                    case 15 : output.insert(0,'F'); break;
                    default : output.insert(0,remainder); break;
                }
                value = newValue;
            }
            else{            
                output.insert(0,remainder);                        
                value = newValue;
            }        
        }
               
        return output.toString();        
    }    
    
    
    
/** This function converts the given number (as a String) in the system with the 
 *  given radix to an integer (base 10).
 * 
 * @param input - [String] The number to convert
 * @param inputRadix - [int] The radix of the given number
 * @return - [int] The equivalent integer of the given number String
 */    
    public static int convertBASEtoINT(String input, int inputRadix){
        input = input.toUpperCase();
        char[] inputArray = input.toCharArray();
        double exponent = 0;
        int output = 0;
        
        for(int i = inputArray.length-1; i >= 0; i--){
            
            int digit = (int)inputArray[i];
            int value;
            
            if(digit >= 48 && digit <= 57 && digit-48 < inputRadix){
                value = digit - 48;
            }
            else if(digit >= 65 && digit <= 70 && inputRadix == 16){
                value = digit - 55;
            }
            else{
                value = 0;
            }

            int sum = (int)(value * Math.pow(inputRadix,exponent));
            
            output += sum;
            exponent++;            
        }
        
        return output;
    }
    
/** This function converts the given number (as a String) in the system with the 
 *  given radix to a double (base 10).
 * 
 * @param input - [String] The number to convert
 * @param inputRadix - [int] The radix of the given number
 * @return - [double] The equivalent double of the given number String
 */        
    public static double convertBASEtoDOUBLE(String input, int inputRadix){                       
        input = input.toUpperCase();
        String[] parts = splitDecimal(input);
        String integerPart = parts[0];
        char[] fractionPart = parts[1].toCharArray();
        
        int exponent = -1;
        BigDecimal total = new BigDecimal("0.0");
        
        for(int i = 0; i < fractionPart.length; i++){
            int ascii = (int)fractionPart[i];
            int value;
            
            if(ascii == 46 && i == fractionPart.length - 3){
                break;
            }
            
            if(ascii >= 48 && ascii <= 57 && ascii-48 < inputRadix){
                value = ascii - 48;
            }
            else if(ascii >= 65 && ascii <= 70 && inputRadix == 16){
                value = ascii - 55;
            }
            else{
                value = 0;
            }
            
            BigDecimal sum = new BigDecimal(""+(value * Math.pow(inputRadix,exponent)));
            total = total.add(sum);
            
            exponent--;
        }
           
        BigDecimal output = new BigDecimal(convertBASEtoINT(integerPart,inputRadix));
        output = output.add(total);        
        
        return output.doubleValue();
        
    }
    
/** This function converts the given number (as a String) in the system with the 
 *  given radix to a long (base 10).
 * 
 * @param input - [String] The number to convert
 * @param inputRadix - [int] The radix of the given number
 * @return - [long] The equivalent long of the given number String
 */        
    public static long convertBASEtoLONG(String input, int inputRadix){
        input = input.toUpperCase();
        char[] inputArray = input.toCharArray();
        double exponent = 0;
        long output = 0;
        
        for(int i = inputArray.length-1; i >= 0; i--){
            
            int digit = (int)inputArray[i];
            int value;
            
            if(digit >= 48 && digit <= 57 && digit-48 < inputRadix){
                value = digit - 48;
            }
            else if(digit >= 65 && digit <= 70 && inputRadix == 16){
                value = digit - 55;
            }
            else{
                value = 0;
            }

            long sum = (long)(value * Math.pow(inputRadix,exponent));
            
            output += sum;
            exponent++;            
        }
        
        return output;
    }        
    
    
    
/** This function separates the given String into an array of Strings that 
 *  represent numbers, similar in regards to a split String method, though this
 *  uses any non-numeric character as a delimiter.
 * 
 * @param input - [String] The String to split
 * @return - [String[]] An array of number and symbol Strings
 */    
    public static String[] separateNumString(String input){
        
        char[] inputArray = input.toCharArray();
        StringBuilder output = new StringBuilder();

        
        for(int i = 0; i < inputArray.length; i++){            
            int ascii = (int)inputArray[i];
            
            /*if(ascii == 46 && i != inputArray.length - 1){
                // IF THE CURRENT CHARACTER IS A FULL STOP AND IT HAS A NUMBER
                // BEHIND IT, WE ADD IT AS A DECIMAL, OTHERWISE, WE DO NOT.
                
                if((int)inputArray[i+1] >= 48 && (int)inputArray[i+1] <= 57){
                    output.append(inputArray[i]);
                    continue;
                }
                else{
                    output.append(" "+inputArray[i]+" ");
                    continue;
                }
                
            }*/
            
            if(!(ascii >= 48 && ascii <= 57 || ascii >= 65 && ascii <= 70 || ascii >= 97 && ascii <= 102)){
                // IF THE CURRENT CHARACTER IS NOT A NUMBER OR LETTER A TO F, WE 
                // ISOLATE IT
                output.append(" "+inputArray[i]+" ");
            }
            else{
                output.append(inputArray[i]);
            }
        }
        
        // ITERATE THROUGH THE ARRAY AND REMOVE ANY INSTANCES OF EMPTY CELLS
        
        String[] separatedString = output.toString().split(" ");
        String[] trimmedOutput;
        int count = 0;
        
        for(String current : separatedString){
            if(current.equals("")){
                count++;
            }
        }
        
        trimmedOutput = new String[separatedString.length-count];
        int j = 0;
        
        for(int i = 0; i < separatedString.length; i++){
            if(separatedString[i].equals("")){
                continue;
            }
            else{
                trimmedOutput[j] = separatedString[i];
                j++;
            }
        }
        
        return trimmedOutput;
    }



/** This function performs Euclid's GCD algorithm to the two given integer 
 *  values and returns the output.
 * 
 * @param a - [int] The first integer input of the GCD algorithm
 * @param b - [int] The second integer input of the GCD algorithm
 * @return - [int] The greatest common divisor between the two integer inputs
 */        
    public static int greatestCommonDivisor(int a, int b){
        if(a < 0 || b < 0){
            return 0;
        }
        else{
            
            if(!(a >= b)){
                int c = b;
                b = a;
                a = c;
            }
            
// a has to be bigger than or equal to b. If this is not the case, the two integers
// are swapped.
            
            while(b != 0){
                int r = a % b;
                a = b;
                b = r;
            }
            return a;
        }        
    }    
        
/** This function performs Euclid's GCD algorithm to the two given long 
 *  values and returns the output.
 * 
 * @param a - [long] The first long input of the GCD algorithm
 * @param b - [long] The second long input of the GCD algorithm
 * @return - [long] The greatest common divisor between the two long inputs
 */    
    public static long greatestCommonDivisor(long a, long b){
        if(a < 0 || b < 0){
            return 0;
        }
        else{
            
            if(!(a >= b)){
                long c = b;
                b = a;
                a = c;
            }
            
// a has to be bigger than or equal to b. If this is not the case, the two integers
// are swapped.
            
            while(b != 0){
                long r = a % b;
                a = b;
                b = r;
            }
            return a;
        }        
    }

    
/** This function performs Euclid's extended algorithm, in which the greatest
 *  common divisor, d, is calculated along with values x and y which satisfy:
 *  ax + by = d, which are returned in a long array.
 * 
 * @param a - [long] The first operand of the algorithm
 * @param b - [long] The second operand of the algorithm
 * @return = [long] An array containing the values of d, x and y (in that order)
 */        
    public static int[] extendedEuclideanAlgorithm(int a, int b){
        
        int[] output = {0,0,0};
// an integer array that will hold the greatest common divisor, d, and the values
// of x and y which satisfy:
//
// ax + by = d;
//
// d will be held at the first index (0), x at the second (1) and y at the third (2).

        
        int x, y, d, q, r, x1, x2, y1, y2;
        
        if(a < 0 || b < 0){
            return output;
        }
        else{
            
            if(!(a >= b)){
                int c = b;
                b = a;
                a = c;
            }
            
// a has to be bigger than or equal to b. If this is not the case, the two integers
// are swapped.
        
            if(b == 0){
                output[0] = a;
                output[1] = 1;
                output[2] = 0;
                
                return output;
            }
            else{
                x1 = 0; 
                x2 = 1;
                y1 = 1;
                y2 = 0;
            }
            
            while(b > 0){
                q = a / b;
                r = a - (q*b);
                x = x2 - (q*x1);
                y = y2 - (q*y1);
                
                a = b;
                b = r;
                x2 = x1;
                x1 = x;
                y2 = y1;
                y1 = y;
            }
            
            output[0] = a;
            output[1] = x2;
            output[2] = y2;
            
            return output;
        }   
    }
    
/** This function performs Euclid's extended algorithm, in which the greatest
 *  common divisor, d, is calculated along with values x and y which satisfy:
 *  ax + by = d, which are returned in an integer array.
 * 
 * @param a - [int] The first operand of the algorithm
 * @param b - [int] The second operand of the algorithm
 * @return = [int] An array containing the values of d, x and y (in that order)
 */    
    public static long[] extendedEuclideanAlgorithm(long a, long b){
        
        long[] output = {0,0,0};
// an integer array that will hold the greatest common divisor, d, and the values
// of x and y which satisfy:
//
// ax + by = d;
//
// d will be held at the first index (0), x at the second (1) and y at the third (2).

        
        long x, y, d, q, r, x1, x2, y1, y2;
        
        if(a < 0 || b < 0){
            return output;
        }
        else{
            
            if(!(a >= b)){
                long c = b;
                b = a;
                a = c;
            }
            
// a has to be bigger than or equal to b. If this is not the case, the two integers
// are swapped.
        
            if(b == 0){
                output[0] = a;
                output[1] = 1;
                output[2] = 0;
                
                return output;
            }
            else{
                x1 = 0; 
                x2 = 1;
                y1 = 1;
                y2 = 0;
            }
            
            while(b > 0){
                q = a / b;
                r = a - (q*b);
                x = x2 - (q*x1);
                y = y2 - (q*y1);
                
                a = b;
                b = r;
                x2 = x1;
                x1 = x;
                y2 = y1;
                y1 = y;
            }
            
            output[0] = a;
            output[1] = x2;
            output[2] = y2;
            
            return output;
        }   
    }    

    
}