package ProjectPackage.MODEL;

/** This interface is the superclass that represents all ciphers implemented in
 *  this program. A majority of the functions, while implemented, are not often
 *  necessary, particularly the functions which encrypt and decrypt to and from
 *  text files.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public abstract interface Cipher {
      
    
/** This function is to encrypt a Message class object using the algorithm of
 *  the subclass which overrides it.
 * 
 *  @param input - [Message] The Message object containing the plaintext
 *  @return - [Message] A Message object containing the ciphertext
 */
    public abstract Message encrypt(Message input);  
    
/** This function is to encrypt a Message class object and saves the output
 *  to a text file at the given file path.
 * 
 *  @param input - [Message] A Message class object containing the plaintext
 *  @param outputFilePath - [String] The location to save the output text file to 
 */
    public abstract void encryptTOfile(Message input, String outputFilePath);

/** This function is to encrypt the text from a text file at the given location 
 *  and return a Message class object containing the ciphertext.
 * 
 *  @param filename - [String] The location of the text file containing the plaintext
 *  @return - [Message] A Message class object containing the ciphertext
 */    
    public abstract Message encryptFROMfile(String filename);            
    
    

/** This function is to decrypt a Message class object using the algorithm of 
 *  the subclass which overrides it.
 * 
 *  @param input [Message] - A Message class object containing the ciphertext
 *  @return - [Message] - A Message class object containing the plaintext
 */    
    public abstract Message decrypt(Message input);    
   
/** This function is to decrypt a Message class object and saves the output 
 *  to a text file at the given file path.
 * 
 *  @param input - [Message] A Message class object containing the ciphertext 
 *  @param outputFilePath - [String] The location to save the output text file to
 */    
    public abstract void decryptTOfile(Message input, String outputFilePath);    
   
/** This function is to decrypt the text from a text file at the given location 
 *  and return a Message class object containing the plaintext.
 * 
 * @param filename - [String] The location of the text file containing the ciphertext
 * @return - [Message] A Message class object containing the plaintext
 */    
    public abstract Message decryptFROMfile(String filename);
    
    

/** This function is to return a String containing the information of the 
 *  invoking subclass object instance, such information includes the plaintext,
 *  ciphertext and parameters, mainly for use in the ProjectProgram class.
 * 
 * @param plaintext - [String] The plaintext to be encrypted by the invoking Cipher
 * @param ciphertext - [String] The ciphertext encrypted by the invoking Cipher
 * @param format - [String] The format of the input (Text, ASCII, Binary, Octal, Hexadecimal)
 * @param encrypting - [boolean] Whether encryption is occurring or not.
 * @return - [String] A String containing all information on the invoking Cipher
 */    
    public abstract String printDetails(String plaintext, String ciphertext, String format, boolean encrypting);            
    
}