package ProjectPackage.CONTROLLER;
import ProjectPackage.MODEL.*;

/** This class represents a symmetric key cipher, or a cipher uses the same key, 
 *  for both encryption and decryption, this implementation includes the RC4
 *  algorithm.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public class RC4 implements Cipher{

// FIELDS    
    
    private String password;
    private boolean capitals;
    private int[] keyStream;
    
// CONSTRUCTORS    
          
    public RC4(String input){
        password = input;
        capitals = false;
        keyStream = generateKeyStream();        
    }
    
    public RC4(String passInput, boolean capitalsAllowed){
        password = passInput;
        capitals = capitalsAllowed;   
        keyStream = generateKeyStream();        
    }
    
    public RC4(String passInput, boolean capitalsAllowed, boolean symbolsAllowed){
        password = passInput;
        capitals = capitalsAllowed;
        keyStream = generateKeyStream();        
    }    
    
// METHODS    
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts the passed Message input according to the invoking object's current
 *  properties and returns the ciphertext as a new Message class object.
 * 
 * @param input - [Message] The Message containing the plaintext to encrypt
 * @return - [Message] A Message class object containing the encrypted text
 */       
    @Override
    public Message encrypt(Message input){               
        keyStream = generateKeyStream();        
                
        if(!capitals){
            input.lowercase();
        }      
                
        return symmetricEncryption(input,true);        
    }                
        
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts the passed Message input and saves it to a text file in the given
 *  file path location.
 * 
 * @param input - [Message] The Message containing the plaintext
 * @param outputFilePath - [String] The location to save the output file to 
 */     
    @Override
    public void encryptTOfile(Message input, String outputFilePath){
        
    }       
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts text from a file at the give location and returns it as a new 
 *  Message object.
 * 
 * @param filename - [String] The location of the file to read from
 * @return - [Message] A Message class object containing the encrypted text
 */     
    @Override
    public Message encryptFROMfile(String filename){
        return new Message();
    }
                
    
    
/** This function overrides the relevant method in the Cipher interface, it
 *  decrypts the passed Message input according to the invoking object's current
 *  properties and returns the ciphertext as a new Message class object.
 * 
 * @param input - [Message] The Message containing the ciphertext to decrypt
 * @return - [Message] A Message class object containing the decrypted text
 */        
    @Override
    public Message decrypt(Message input){     
        keyStream = generateKeyStream();            
        
        return symmetricEncryption(input,false);
        
        // BECAUSE RC4 IS A SYMMETRIC KEY CIPHER, THE ENCRYPTION AND DECRYPTION
        // METHODS ARE IDENTICAL. WE DO NOT, HOWEVER, WANT TO ADJUST THE 
        // CASES OF THE INPUT AT THIS POINT, BECAUSE IT WILL CHANGE THE
        // VALUES OF THE OUTPUT UNEXPECTEDLY.
    }
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  decrypts the passed Message input according to the invoking object's current
 *  properties and saves the output to a text file at the specified location. 
 * 
 * @param input - [Message] The Message object containing the ciphertext
 * @param outputFilePath - [String] The location to save the output to
 */      
    @Override
    public void decryptTOfile(Message input, String outputFilePath){
        
    }   
    
/** This function overrides the relevant method in the Cipher interface, it
 *  decrypts the text from a text file at the passed location and returns the
 *  output as a new Message class object.
 * 
 * @param filename - [String] The location of the file to read from
 * @return - [Message] A Message containing the decrypted plaintext
 */      
    @Override
    public Message decryptFROMfile(String filename){
        return new Message();
    }
    
    
    
/** This function is the function which is used to both encrypt and decrypt
 *  due to RC4 being a symmetric key cipher, the boolean value passed 
 *  indicates whether encryption or decryption is taking place, so certain 
 *  variables, such as the keystream, are not overwritten.
 * 
 * @param input - [Message] The Message which contains the plaintext or ciphertext
 * @param encryption - [boolean] This indicates if encryption is occurring or not
 * @return 
 */    
    public Message symmetricEncryption(Message input, boolean encryption){        

        int[] inputArray = input.getCodes();
        
        int[] output = new int[inputArray.length];
        
        int a = 0;
        int b = 0;
        
        for(int x = 0; x < inputArray.length; x++){
            int c = (a + 1) % 256;
            a = c;
            
            int d = (b + keyStream[a]) % 256;            
            b = d;
            
            int X = keyStream[a];
            int Y = keyStream[b];
            
            keyStream[a] = Y;
            keyStream[b] = X;
            
            int Z1 = keyStream[(keyStream[a] + keyStream[b]) % 256];                  
            
            String Z2 = Digits.convertINTtoBASE(Z1,2,8);
            String W2 = Digits.convertINTtoBASE(inputArray[x],2,8);
            
            String answer = Bitwise.XOR(Z2,W2);
            
            int P = Digits.convertBASEtoINT(answer,2);

            output[x] = P;
        }
        
        Message M = new Message();
        M.setCodes(output);
        
        return M;                
    }
    
/** This function randomises a keystream containing each permutation of an 
 *  8 bit binary number depending on the invoking object's password field.
 * 
 * @return - [int[]] A 256 integer array, randomised to the password field
 */    
    public final int[] generateKeyStream(){
        char[] stringKEY = this.password.toCharArray();
        int[] intKEY = new int[stringKEY.length];
        
        for(int i = 0; i < stringKEY.length; i++){
            intKEY[i] = (int)(stringKEY[i]);
        }
        // THE KEY STRING IS SPLIT INTO A CHARACTER ARRAY AND THEN EACH ELEMENT 
        // IS CONVERTED TO ITS ASCII EQUIVALENT AND STORED IN A NEW ARRAY.        
        
        int[] STREAM = new int[256];      
        
        for(int a = 0; a < STREAM.length; a++){
            STREAM[a] = a;
        }
        // THIS HOLDS ALL PERMUTATIONS OF AN 8-BIT NUMBER.
        
        int i = 0;
        int j = 0;
        
        for(i = 0; i < 256; i++){
        
            int X = i % intKEY.length;
            int k = ( (j + STREAM[i] + intKEY[X]) % 256 );
            
            int Y = STREAM[i];
            int Z = STREAM[k];
            
            STREAM[i] = Z;
            STREAM[k] = Y; 
            
            // THE ith AND jth ELEMENTS OF THE KEYSTREAM ARE SWAPPED, THE TWO 
            // VALUES, i AND j ARE THEN UPDATED ACCORDINGLY.
            
            j = k;
        }  
        
        return STREAM;
    }    
    
/** This function returns the password field of the invoking object.
 * 
 * @return - [String] The password of the invoking object
 */    
    public String getPassword(){
        return password;
    }    
    
/** This function returns the capitals field of the invoking object.
 * 
 * @return - [boolean] The capitals field of the invoking object
 */    
    public boolean capitalsAllowed(){
        return capitals;
    }    
        
    

/** This function assigns the invoking object's password field to the passed
 *  String.
 * 
 * @param input - [String] The new password value to assign
 */    
    public void setPassword(String input){
        password = input;
    }
    
/** This function assigns the invoking object's capitals field to the passed
 *  boolean value.
 * @param input - [boolean] The new capitals value to assign
 */    
    public void setCapitalsAllowed(boolean input){
        capitals = input;
    }
    
    
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  returns a String containing the information of the invoking object along 
 *  with the passed Strings.
 * 
 * @param plaintext - [String] A String representing the plaintext to encrypt
 * @param ciphertext - [String] A String representing the encrypted ciphertext
 * @param format - [String] A String representing the format of the plaintext
 * @param encrypting - [boolean] An indication of if encryption is occurring or not
 * @return - [String] A String containing all relevant information
 */        
    @Override    
    public String printDetails(String plaintext, String ciphertext, String format, boolean encrypting){
        StringBuilder output = new StringBuilder();

        output.append("\n------------------------------------------------------------\n");
        
        if(encrypting){
            output.append("ENCRYPTING PLAINTEXT ["+plaintext+"]\n");
        }
        else{
            output.append("DECRYPTING PLAINTEXT ["+plaintext+"]\n");            
        }
            
        output.append("USING: RC4 CIPHER\n");
        output.append("INPUT:\n");

        if(format.equals("Text") || format.equals("ASCII")){
            output.append("\tTEXTUAL INPUT ("+format+")\n");                                        
        }
        else{
            output.append("\tNUMBERICAL INPUT ("+format+")\n");
        }   

        output.append("\nPARAMETERS:\n");        
        
        output.append("\tPASSWORD : "+password+"\n");
                        
        if(capitals){
            output.append("\tCAPITALS ACCEPTED\n");
        }
        else{
            output.append("\tCAPITALS NOT ACCEPTED\n");
        }      
        
        if(encrypting){
            output.append("ENCRYPTION COMPLETE ["+ciphertext+"]\n");        
        }
        else{
            output.append("DECRYPTION COMPLETE ["+ciphertext+"]\n");
        }            

        output.append("------------------------------------------------------------\n");        
        
        return output.toString();
    }   

    
}