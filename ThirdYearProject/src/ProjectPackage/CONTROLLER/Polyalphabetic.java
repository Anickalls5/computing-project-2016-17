package ProjectPackage.CONTROLLER;
import ProjectPackage.MODEL.*;

/** This class represents a polyalphabetic cipher, or a cipher which uses more than 
 *  one alphabet, this implementation includes the Vigenére cipher.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public class Polyalphabetic implements Cipher{
    
    private String password;
    private boolean capitals;
    
    public Polyalphabetic(){
        password = "";
        capitals = false;
    }
    
    public Polyalphabetic(String keyword){
        password = keyword;
        capitals = false;
    }
    
    public Polyalphabetic(String keyword, boolean capitalsAllowed){
        password = keyword;
        capitals = capitalsAllowed;
    }
    
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts the passed Message input according to the invoking object's current
 *  properties and returns the ciphertext as a new Message class object.
 * 
 * @param input - [Message] The Message containing the plaintext to encrypt
 * @return - [Message] A Message class object containing the encrypted text
 */        
    @Override
    public Message encrypt(Message input){        
        String inputString = input.getText();
        String key = this.getPassword().toLowerCase();
        
        if(!this.capitals){
            inputString = input.getText().toLowerCase();
        }
        
        char[][] square = generateSquare();       
        char[] inputCharacters = inputString.toCharArray();
        char[] keyCharacters = key.toCharArray();
        
        // THE VIGENERE SQUARE IS GENERATED
        
        int index = 0;
        StringBuilder output = new StringBuilder();
        
        for(char currentChar : inputCharacters){            
            char keyChar = keyCharacters[index];

            int K, P;
            
            if((int)keyChar >= 65 && (int)keyChar <= 90){
                K = (int)(keyChar - 65);
            }
            else if((int)keyChar >= 97 && (int)keyChar <= 122){
                K = (int)(keyChar - 97);
            }
            else{
                throw new IllegalArgumentException("LETTERS ONLY FOR PASSWORD");
            }
            // ONLY LETTERS FOR THE PASSWORD WILL BE ACCEPTED
            
            if((int)currentChar >= 65 && (int)currentChar <= 90){
                P = (int)(currentChar - 65);
            }
            else if((int)currentChar >= 97 && (int)currentChar <= 122){
                P = (int)(currentChar - 97);
            }
            else{
                output.append(""+currentChar);
                continue;
            }
            
// FOR EVERY CHARACTER, FIND THE CIPHERTEXT VALUE IN THE TABLE, REMEMBER THAT
// ROWS REPRESENT THE PLAINTEXT CHARACTER AND THE COLUMNS REPRESENT THE 
// KEY CHARACTER. THE INPUT SHOULD BE TESTED FOR VALIDITY BEFOREHAND, THOUGH
// ANY NON-LETTER SYMBOLS ARE NOT CONVERTED.
            
            char X = square[P][K];
            index = (index+1) % keyCharacters.length;
            
            if((int)currentChar >= 65 && (int)currentChar <= 90){
                // IF THE CURRENT LETTER IS A CAPITAL LETTER, WE NEED TO 
                // ENSURE THAT THE OUTPUT IS TOO
                
                X = (char)((int)X - 32);
            }            
            
            output.append(X);
        }
        
        return new Message(output.toString());        
    }
        
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts the passed Message input and saves it to a text file in the given
 *  file path location.
 * 
 * @param input - [Message] The Message containing the plaintext
 * @param outputFilePath - [String] The location to save the output file to 
 */        
    @Override
    public void encryptTOfile(Message input, String outputFilePath){
        
    }
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts text from a file at the give location and returns it as a new 
 *  Message object.
 * 
 * @param filename - [String] The location of the file to read from
 * @return - [Message] A Message class object containing the encrypted text
 */     
    @Override
    public Message encryptFROMfile(String filename){
        Message M = new Message();
        return M;
    }
    
    
    
/** This function overrides the relevant method in the Cipher interface, it
 *  decrypts the passed Message input according to the invoking object's current
 *  properties and returns the ciphertext as a new Message class object.
 * 
 * @param input - [Message] The Message containing the ciphertext to decrypt
 * @return - [Message] A Message class object containing the decrypted text
 */        
    @Override
    public Message decrypt(Message input){
        String inputString = input.getText();
        String keyword = this.getPassword().toLowerCase();
        
        if(!this.capitals){
            inputString = input.getText().toLowerCase();
        }
        
        char[] inputCharacters = inputString.toCharArray();
        char[] keyCharacters = keyword.toCharArray();
        char[][] square = generateSquare();
        String output = "";
        int j = 0;
        
        
        for(int i = 0; i < inputCharacters.length; i++){
            for(int x = 0; x < square.length; x++){                                    
                
                int cipherCode = (int)inputCharacters[i];
                int passCode = (int)keyCharacters[j % keyCharacters.length] - 97;
                
                if(!(cipherCode >= 65 && cipherCode <= 90 || cipherCode >= 97 && cipherCode <= 122)){
                    output = output + (char)cipherCode;
                    break;
                }
                
                int plainCode = (int)square[x][passCode];
                
                if(this.capitals && cipherCode >= 65 && cipherCode <= 90){
                    plainCode = plainCode - 32;
                }
                
                if(cipherCode == plainCode){
                    if(capitals && cipherCode >= 65 && cipherCode <= 90){
                        output = output + (char)(x + 65);
                    }
                    else{
                        output = output + (char)(x + 97);
                    }
                    j++;
                    break;
                }                  
            }
        }            
        
        return new Message(output);
    }    

/** This function overrides the relevant method in the Cipher interface, it 
 *  decrypts the passed Message input according to the invoking object's current
 *  properties and saves the output to a text file at the specified location. 
 * 
 * @param input - [Message] The Message object containing the ciphertext
 * @param outputFileName - [String] The location to save the output to
 */       
    @Override
    public void decryptTOfile(Message input, String outputFileName){
        
    }
    
/** This function overrides the relevant method in the Cipher interface, it
 *  decrypts the text from a text file at the passed location and returns the
 *  output as a new Message class object.
 * 
 * @param filename - [String] The location of the file to read from
 * @return - [Message] A Message containing the decrypted plaintext
 */     
    @Override
    public Message decryptFROMfile(String filename){
        Message M = new Message();
        return M;
    }
    
    
    
/** This function generates a Vigenére square and returns it as a two-dimensional
 *  array of characters.
 * 
 * @return - [char[][]] The generated square
 */    
    public char[][] generateSquare(){
        int a = 0;        
        char[][] square = new char[26][26];
        
        for(int i = 0; i<= 25; i++){            
            int b = 0;
        
            for(int j = i; j <= i+25; j++){
                int k = j%26;
                square[a][b] = (char)(k+97);                
                b++;                
            }
            
            a++;                   
        }
        
        return square;        
    }
    
    
/** This function returns the invoking object's password field.
 * 
 * @return - [String] The password field of the invoking object.
 */    
    public String getPassword(){
        return password;
    }

/** This function returns the invoking object's capitals field.
 * 
 * @return - [boolean] The invoking object's capitals field
 */    
    public boolean capitalsAllowed(){
        return capitals;
    }

/** This function assigns the invoking object's password field to the passed
 *  String.
 * @param newPass 
 */    
    public void setPassword(String newPass){
        password = newPass;
    }
    
/** This function assigns the invoking object's capitals field to the passed
 *  boolean value.
 * 
 * @param newSetting - [boolean] The new boolean value 
 */    
    public void setCapitalsAllowed(boolean newSetting){
        capitals = newSetting;
    }
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  returns a String containing the information of the invoking object along 
 *  with the passed Strings.
 * 
 * @param plaintext - [String] A String representing the plaintext to encrypt
 * @param ciphertext - [String] A String representing the encrypted ciphertext
 * @param format - [String] A String representing the format of the plaintext
 * @param encrypting - [boolean] An indication of if encryption is occurring or not
 * @return - [String] A String containing all relevant information
 */       
    @Override
    public String printDetails(String plaintext, String ciphertext, String format, boolean encrypting){
        StringBuilder output = new StringBuilder();

        output.append("\n------------------------------------------------------------\n");
        
        if(encrypting){
            output.append("ENCRYPTING PLAINTEXT ["+plaintext+"]\n");
        }
        else{
            output.append("DECRYPTING PLAINTEXT ["+plaintext+"]\n");            
        }           

        output.append("USING: POLYALPHABETIC CIPHER\n");
        output.append("INPUT:\n");

        if(format.equals("Text") || format.equals("ASCII")){
            output.append("\tTEXTUAL INPUT ("+format+")\n");                                        
        }
        else{
            output.append("\tNUMBERICAL INPUT ("+format+")\n");
        }   

        output.append("\nPARAMETERS:\n");                                                                                                           
        
        output.append("\tPASSWORD : "+password+"\n");  
        
        if(capitals){
            output.append("\tCAPITALS ACCEPTED\n");
        }
        else{
            output.append("\tCAPITALS NOT ACCEPTED\n");
        }
        
        if(encrypting){
            output.append("\nENCRYPTION COMPLETE ["+ciphertext+"]\n");        
        }
        else{
            output.append("\nDECRYPTION COMPLETE ["+ciphertext+"]\n");
        }            

        output.append("------------------------------------------------------------\n");        
        
        return output.toString();
    }
    
    
}