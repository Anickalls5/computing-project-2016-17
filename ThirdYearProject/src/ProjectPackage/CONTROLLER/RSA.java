package ProjectPackage.CONTROLLER;
import ProjectPackage.MODEL.*;
import java.math.BigInteger;
import java.util.Random;
import java.util.stream.LongStream;

/** This class represents an asymmetric key cipher, or a cipher which uses different, 
 *  keys for encryption and decryption, this implementation includes the RSA
 *  algorithm.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public class RSA implements Cipher {    

// FIELDS    
    
    private BigInteger pValue;
    private BigInteger qValue;
    private BigInteger nValue;
    private BigInteger eValue;
    private BigInteger dValue;   
    private boolean capitals;
    
// CONSTRUCTORS    
    
    public RSA(BigInteger[] values){
        pValue = values[0];
        qValue = values[1];
        nValue = values[2];
        eValue = values[3];
        dValue = values[4];  
        
        capitals = false;
    }
    
    public RSA(BigInteger[] values, boolean capitalsAllowed){
        pValue = values[0];
        qValue = values[1];
        nValue = values[2];
        eValue = values[3];
        dValue = values[4];        

        capitals = capitalsAllowed;
    }
    
    public RSA(BigInteger[] values, boolean capitalsAllowed, boolean symbolsAllowed){
        pValue = values[0];
        qValue = values[1];
        nValue = values[2];
        eValue = values[3];
        dValue = values[4];  
        
        capitals = capitalsAllowed;
    }
    

// METHODS    
    
    
    @Override      
    public Message encrypt(Message input){        
        Message ciphertext = new Message();

        String plaintext = input.getText();
        
        if(!capitals){
            plaintext = plaintext.toLowerCase();
        }
        
        char[] inputArray = plaintext.toCharArray();
        BigInteger[] output = new BigInteger[inputArray.length];
                
        for(int i = 0; i < inputArray.length; i++){
            int code = (int)inputArray[i];
            
            BigInteger cipherCode = new BigInteger(""+code);
            BigInteger X = cipherCode.modPow(eValue,nValue);
            
            output[i] = X;
        }
        
        ciphertext.setBigCodes(output);    
        return ciphertext;        
    }  
    
    @Override    
    public void encryptTOfile(Message input, String outputFilePath){
                
    }
    
    @Override    
    public Message encryptFROMfile(String filename){
        return new Message();                
    }
            
    
    
    @Override    
    public Message decrypt(Message input){        
        BigInteger[] ciphertext = input.getBigCodes();        
        BigInteger[] output = new BigInteger[ciphertext.length];
        
        for(int i = 0; i < ciphertext.length; i++){            
            BigInteger code = ciphertext[i];                       
            BigInteger M = code.modPow(dValue,nValue);
            output[i] = M;
        }
        
        return new Message(output);
    }
    
    @Override    
    public void decryptTOfile(Message input, String outputFilePath){
        
    }   
    
    @Override
    public Message decryptFROMfile(String filename){
        return new Message();
    }

    
    
/** This function will return a BigInteger value of the specified number of 
 *  digits in size.
 * 
 * @param numberOfDigits - [int] The size of the number to generate, in digits
 * @return - [BigInteger] The randomly generated value
 */
    public static BigInteger generateNumber(int numberOfDigits){
        Random R = new Random();   
        
        int max = (int)Math.pow(10,numberOfDigits);
        int min = (int)Math.pow(10,numberOfDigits - 1);
        
        int upperBound = max - min;
        
        StringBuilder S = new StringBuilder();
        int o = R.nextInt(upperBound) + min;
        
        S.append(o);
        
        BigInteger output = new BigInteger(S.toString());
        
        return output;
    }    
    
    
/** This function generates the five keys in accordance to the RSA key generation
 *  algorithm and returns a BigInteger array containing them, the output array
 *  is in the following order: P, Q, N, E, D.
 * 
 * @param primeLength - [int] The size of the numbers: P and Q, in digits
 * @return - [BigInteger[]] An array containing the five key values
 */
    public static BigInteger[] generateKeys(int primeLength){
              
        BigInteger[] output = new BigInteger[5];
        
        BigInteger p;
        BigInteger q;
        
        //System.out.println("GENERATING P...");
        
        while(true){
            p = generateNumber(primeLength);
            if(Digits.isPrime(p.longValue()) == true && p.intValue() > 1){
                break;
            }
        }
        
// KEEP GENERATING A NUMBER OF SPECIFIED LENGTH UNTIL A PRIME ONE IS REACHED.        
        
        //System.out.println("GENERATING Q...");

        while(true){
            q = generateNumber(primeLength);

            if( p.compareTo(q) == 1 ){
                double x = q.doubleValue()/p.doubleValue();
                if(x >= 0.6){                    
                    if(Digits.isPrime(q.longValue()) == true && q.intValue() > 1){
                        break;
                    }
                }
            }                  
            else{
                double x = p.doubleValue()/q.doubleValue();
                if(x >= 0.6){
                    if(Digits.isPrime(q.longValue()) == true && q.intValue() > 1){
                        break;
                    }
                }
            }
        }
// KEEP GENERATING A SECOND NUMBER OF SPECIFIED LENGTH UNTIL IT IS BOTH PRIME AND 
// WITHIN A PARTICULAR DISTANCE OF THE FIRST. THE LARGER OF THE TWO GENERATED 
// NUMBERS IS THE NUMERATOR IN THE DIVISION.
        
// to ensure that they are of similar lengths, we divide the smaller number over 
// the larger, if the decimal is not above 0.75, then the difference is too large.
        
        BigInteger n = p.multiply(q);
        
        BigInteger m = new BigInteger("-1");
        
        BigInteger p2 = p.add(m);
        BigInteger q2 = q.add(m);
        
        BigInteger phi = p2.multiply(q2);
        
        //System.out.println("CALCULATING N AND PHI...");
        
        Random generator = new Random();
        BigInteger e;

        //System.out.println("GENERATING E...");        
        
        int count = 0;
        
        while(true){
            try{                
                LongStream LS = generator.longs(1,0,phi.longValue());
                
                // WE GENERATE A STREAM OF RANDOM LONG NUMBERS BETWEEN 0 AND PHI
                // WHICH CONTAINS ONLY ONE ELEMENT. THIS ELEMENT IS THEN USED
                // AS A POSSIBLE VALUE FOR E, BUT ONLY IF IT FULFILS THE CRITERIA                
                
                e = new BigInteger(""+LS.toArray()[0]);                                               

                if(Digits.greatestCommonDivisor(phi.longValue(), e.longValue()) == 1 && e.compareTo(phi) == -1 && e.intValue() > 1){
                    break;
                }    
            }
            catch(IllegalArgumentException E){
                count++;
                if(count >= 100){
                    System.out.println("STUCK");
                }
            }
        }

// KEEP GENERATING A NUMBER UNTIL IT IS RELATIVELY PRIME TO THE VALUE OF PHI.
        
        //System.out.println("CALCULATING D...");

        long[] extendedEuclid = Digits.extendedEuclideanAlgorithm(phi.longValue(), e.longValue());
        long d = extendedEuclid[2];
        
        BigInteger D = new BigInteger(Digits.convertLONGtoSTRING(d));
                
        if( d < 0 ){
            D = D.add(phi);
        }
        
        output[0] = p;
        output[1] = q;
        output[2] = n;
        output[3] = e;
        output[4] = D;
        
        return output;
    }

/** This function generates the remaining keys from two provided values for P
 *  and Q, which are passed as BigIntegers.
 * 
 * @param p - [BigInteger] The value of P to generate N, E and D from
 * @param q - [BigInteger] The value o Q to generate N, E and D from
 * @return - [BigInteger] An array containing the five key values
 */    
    public static BigInteger[] generateKeys(BigInteger p, BigInteger q){     
                
        if(!Digits.isPrime(p.longValue()) || !Digits.isPrime(q.longValue())){
        //    throw new NumberFormatException("VALUES ARE NOT PRIME");
        }
        
        if( p.compareTo(q) == 1 ){
            double x = q.doubleValue()/p.doubleValue();
            if(x < 0.6){                    
            //    throw new IllegalArgumentException("VALUES ARE TOO FAR APART");                
            }
        }                  
        else{
            double x = p.doubleValue()/q.doubleValue();
            if(x < 0.6){
            //    throw new IllegalArgumentException("VALUES ARE TOO FAR APART");                 
            }
        }        
        
        BigInteger[] output = new BigInteger[5];
        
        BigInteger n = p.multiply(q);

        BigInteger p2 = p.add(new BigInteger("-1"));
        BigInteger q2 = q.add(new BigInteger("-1"));
        
        BigInteger phi = p2.multiply(q2);
        
        //System.out.println("CALCULATING N AND PHI...");
        
        Random generator = new Random();
        BigInteger e;

        //System.out.println("GENERATING E...");        
        
        int count = 0;
        
        while(true){
            try{                
                LongStream LS = generator.longs(1,0,phi.longValue());
                
                // WE GENERATE A STREAM OF RANDOM LONG NUMBERS BETWEEN 0 AND PHI
                // WHICH CONTAINS ONLY ONE ELEMENT. THIS ELEMENT IS THEN USED
                // AS A POSSIBLE VALUE FOR E, BUT ONLY IF IT FULFILS THE CRITERIA                
                
                e = new BigInteger(""+LS.toArray()[0]);                                               

                if(Digits.greatestCommonDivisor(phi.longValue(), e.longValue()) == 1 && e.compareTo(phi) == -1 && e.intValue() > 1){
                    break;
                }    
            }
            catch(IllegalArgumentException E){
                count++;
                if(count >= 100){
                    System.out.println("STUCK");
                }
            }
        }

// KEEP GENERATING A NUMBER UNTIL IT IS RELATIVELY PRIME TO THE VALUE OF PHI.
        
        //System.out.println("CALCULATING D...");

        long[] extendedEuclid = Digits.extendedEuclideanAlgorithm(phi.longValue(), e.longValue());
        long d = extendedEuclid[2];
        
        BigInteger D = new BigInteger(Digits.convertLONGtoSTRING(d));
                
        if( d < 0 ){
            D = D.add(phi);
        }
        
        output[0] = p;
        output[1] = q;
        output[2] = n;
        output[3] = e;
        output[4] = D;
        
        return output;
    }
        
    
    
/** This function returns an array containing the five key values of the invoking
 *  object (P,Q,N,E,D).
 * 
 * @return - [int[]] An array containing the five key values
 */    
    public int[] getVariables(){        
        int[] output = new int[5];
        
        output[0] = pValue.intValue();
        output[1] = qValue.intValue();
        output[2] = nValue.intValue();
        output[3] = eValue.intValue();
        output[4] = dValue.intValue();
        
        return output;
    }

    /** This function returns the capitals field of the invoking object.
     * 
     * @return - [boolean] The capitals field of the invoking object
     */
    public boolean capitalsAllowed(){
        return capitals;
    }   
    
    
/** This function assigns each of the key values to the values in the passed 
 *  array in the order: P,Q,N,E,D.
 * 
 * @param variables - [int[]] The array holding the new values for the keys
 */
    public void setVariables(int[] variables){
        pValue = new BigInteger(""+variables[0]);
        qValue = new BigInteger(""+variables[1]);
        nValue = new BigInteger(""+variables[2]);
        eValue = new BigInteger(""+variables[3]);
        dValue = new BigInteger(""+variables[4]);        
    }
    
/** This function generates the remaining three key values from the two passed
 *  integers, representing the values of P and Q.
 * 
 * @param P - [int] The value of P that will be used to generate N, E and D
 * @param Q - [int] The value of Q that will be used to generate N, E and D
 */    
    public void setVariables(int P, int Q){
        pValue = new BigInteger(""+P);
        qValue = new BigInteger(""+Q);
        
        BigInteger[] values = generateKeys(Digits.countDigits(P));
        
        nValue = values[0];
        eValue = values[1];
        dValue = values[2];
    }

/** This function assigns the capitals field of the invoking object to the 
 *  passed boolean value.
 * 
 * @param input - [boolean] The new value of the capitals field
 */    
    public void setCapitalsAllowed(boolean input){
        capitals = input;        
    }
    
    
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  returns a String containing the information of the invoking object along 
 *  with the passed Strings.
 * 
 * @param plaintext - [String] A String representing the plaintext to encrypt
 * @param ciphertext - [String] A String representing the encrypted ciphertext
 * @param format - [String] A String representing the format of the plaintext
 * @param encrypting - [boolean] An indication of if encryption is occurring or not
 * @return - [String] A String containing all relevant information
 */        
    @Override    
    public String printDetails(String plaintext, String ciphertext, String format, boolean encrypting){
        StringBuilder output = new StringBuilder();

        output.append("\n------------------------------------------------------------\n");
        
        if(encrypting){
            output.append("ENCRYPTING PLAINTEXT ["+plaintext+"]\n");
        }
        else{
            output.append("DECRYPTING PLAINTEXT ["+plaintext+"]\n");            
        }
            
        output.append("USING: RSA CIPHER\n");
        output.append("INPUT:\n");

        if(format.equals("Text") || format.equals("ASCII")){
            output.append("\tTEXTUAL INPUT ("+format+")\n");                                        
        }
        else{
            output.append("\tNUMBERICAL INPUT ("+format+")\n");
        }   

        output.append("\nPARAMETERS:\n");        

        output.append("\tSIZE OF PRIMES = "+Digits.countDigits(pValue.intValue()));
        output.append("\tP = "+pValue+"\n");
        output.append("\tQ = "+qValue+"\n");
        output.append("\tN = "+nValue+"\n");   
        output.append("\tE = "+eValue+"\n");
        output.append("\tD = "+dValue+"\n");
                        
        if(capitals){
            output.append("\tCAPITALS ACCEPTED\n");
        }
        else{
            output.append("\tCAPITALS NOT ACCEPTED\n");
        }         
        
        if(encrypting){
            output.append("ENCRYPTION COMPLETE ["+ciphertext+"]\n");        
        }
        else{
            output.append("DECRYPTION COMPLETE ["+ciphertext+"]\n");
        }            

        output.append("------------------------------------------------------------\n");        
        
        return output.toString();        
    }
    
    
}