package ProjectPackage.CONTROLLER;
import ProjectPackage.MODEL.*;
import java.util.Random;
import java.io.PrintWriter;
import java.io.IOException;

/** This class represents a monoalphabetic cipher, or a cipher which uses only, 
 *  one alphabet, this implementation includes both shift and substitution ciphers.
 * 
 *  @author A.Nickalls {100088688} University of East Anglia 2017
 *  @version 1.0.0
 */
public class Monoalphabetic implements Cipher {

// FIELDS    
    
    private int shiftValue;
    private boolean ordered;
    private boolean capitals;
    private char[] alphabet;
    
// CONSTRUCTORS    
    
    public Monoalphabetic(){
        shiftValue = 0;
        ordered = true;
        capitals = false;
    }
    
    public Monoalphabetic(int shiftInput, boolean orderedInput, boolean capitalsInput){
        shiftValue = shiftInput;
        ordered = orderedInput;
        capitals = capitalsInput;
    }
    
    public Monoalphabetic(int shiftInput, boolean orderedInput, boolean capitalsInput, char[] alphabetInput){
        shiftValue = shiftInput;
        ordered = orderedInput;
        capitals = capitalsInput;
        alphabet = alphabetInput;
    }
           
// METHODS    
    
/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts the passed Message input according to the invoking object's current
 *  properties and returns the ciphertext as a new Message class object.
 * 
 * @param input - [Message] The Message containing the plaintext to encrypt
 * @return - [Message] A Message class object containing the encrypted text
 */    
    @Override
    public Message encrypt(Message input){
        Message output = new Message();
        String plaintext = input.getText();
        
        if(this.capitals == false){
            plaintext = plaintext.toLowerCase();
        }
        
        char[] inputArray = plaintext.toCharArray();
        
        if(this.ordered == true){                   
            setAlphabet(shiftValue);            
            int[] asciiCodes = new int[inputArray.length];

            for(int i = 0; i < inputArray.length; i++){
                asciiCodes[i] = (int)this.shiftCharacter(inputArray[i], shiftValue);
            }

            output.setCodes(asciiCodes);
        }
        else{
            
// USE SET ALPHABET          

            int[] asciiCodes = new int[inputArray.length];
            
            for(int i = 0; i < inputArray.length; i++){                
                int code = (int)inputArray[i];
                int value;
                
                if(code >= 97 && code <= 122){
                    value = code - 97; 
                }
                else if(code >= 65 && code <= 90){   
                    value = code - 65;
                }
                else{
                    asciiCodes[i] = code;
                    continue;
                }
                
                char outputChar = alphabet[value];

                if(capitals == true && code >= 65 && code <= 90){
                    asciiCodes[i] = (int)outputChar - 32;
                }
                else{
                    asciiCodes[i] = (int)outputChar;
                }                            
            }            
            output.setCodes(asciiCodes);            
        }
                
        return output;
    }

/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts the passed Message input and saves it to a text file in the given
 *  file path location.
 * 
 * @param input - [Message] The Message containing the plaintext
 * @param outputFilePath - [String] The location to save the output file to 
 */    
    @Override
    public void encryptTOfile(Message input, String outputFilePath){
        
        try{
            PrintWriter PW = new PrintWriter(outputFilePath);
            
            Message output = encrypt(input);
            PW.append(output.getText());
            PW.flush();
        }
        catch(IOException E){
            System.out.println(E);
        }
        
        
    }

/** This function overrides the relevant method in the Cipher interface, it 
 *  encrypts text from a file at the give location and returns it as a new 
 *  Message object.
 * 
 * @param filename - [String] The location of the file to read from
 * @return - [Message] A Message class object containing the encrypted text
 */    
    @Override
    public Message encryptFROMfile(String filename){
        return null;
    }
    
    

/** This function overrides the relevant method in the Cipher interface, it
 *  decrypts the passed Message input according to the invoking object's current
 *  properties and returns the ciphertext as a new Message class object.
 * 
 * @param input - [Message] The Message containing the ciphertext to decrypt
 * @return - [Message] A Message class object containing the decrypted text
 */    
    @Override
    public Message decrypt(Message input){
        Message output = new Message();
        String ciphertext = input.getText();
        
        if(this.capitals == false){
            ciphertext = ciphertext.toLowerCase();
        }
        
        char[] inputArray = ciphertext.toCharArray();
        
        if(this.ordered == true){                   
            setAlphabet(-1*shiftValue);            
            int[] asciiCodes = new int[inputArray.length];

            for(int i = 0; i < inputArray.length; i++){
                asciiCodes[i] = (int)this.shiftCharacter(inputArray[i], -1*shiftValue);
            }

            output.setCodes(asciiCodes);
        }
        else{
            
// USE SET ALPHABET          

            int[] asciiCodes = new int[inputArray.length];
            
            for(int i = 0; i < inputArray.length; i++){                
                int code = (int)inputArray[i];
                int value;
                
                if(code >= 97 && code <= 122){
                    value = code - 97;  
                }
                else if(code >= 65 && code <= 90){
                    value = code - 65;
                }
                else{
                    asciiCodes[i] = code;
                    continue;
                }
                
                char outputChar = ' ';

                for(int j = 0; j < alphabet.length; j++){                    
                    if(alphabet[j] == (char)(value+65) || alphabet[j] == (char)(value+97)){
                        outputChar = (char)(j+97);
                        break;
                    }
                }

                if(capitals == true && code >= 65 && code <= 90){
                    asciiCodes[i] = (int)outputChar - 32;
                }
                else{
                    asciiCodes[i] = (int)outputChar;
                }
            }            
            output.setCodes(asciiCodes);            
        }
                
        return output;
    }
        
/** This function overrides the relevant method in the Cipher interface, it 
 *  decrypts the passed Message input according to the invoking object's current
 *  properties and saves the output to a text file at the specified location. 
 * 
 * @param input - [Message] The Message object containing the ciphertext
 * @param outputFileName - [String] The location to save the output to
 */    
    @Override 
    public void decryptTOfile(Message input, String outputFileName){

    }    
    
/** This function overrides the relevant method in the Cipher interface, it
 *  decrypts the text from a text file at the passed location and returns the
 *  output as a new Message class object.
 * 
 * @param filename - [String] The location of the file to read from
 * @return - [Message] A Message containing the decrypted plaintext
 */    
    @Override
    public Message decryptFROMfile(String filename){
        return null;
    }
    
    

/** This function overrides the relevant method in the Cipher interface, it 
 *  returns a String containing the information of the invoking object along 
 *  with the passed Strings.
 * 
 * @param plaintext - [String] A String representing the plaintext to encrypt
 * @param ciphertext - [String] A String representing the encrypted ciphertext
 * @param format - [String] A String representing the format of the plaintext
 * @param encrypting - [boolean] An indication of if encryption is occurring or not
 * @return - [String] A String containing all relevant information
 */    
    @Override
    public String printDetails(String plaintext, String ciphertext, String format, boolean encrypting){        
        StringBuilder output = new StringBuilder();

        output.append("\n------------------------------------------------------------\n");
        
        if(encrypting){
            output.append("ENCRYPTING PLAINTEXT ["+plaintext+"]\n");
        }
        else{
            output.append("DECRYPTING PLAINTEXT ["+plaintext+"]\n");            
        }
            
        output.append("USING: MONOALPHABETIC CIPHER\n");
        output.append("INPUT:\n");

        if(format.equals("Text") || format.equals("ASCII")){
            output.append("\tTEXTUAL INPUT ("+format+")\n");                                        
        }
        else{
            output.append("\tNUMBERICAL INPUT ("+format+")\n");
        }   

        output.append("\nPARAMETERS:\n");        
        
        if(ordered){
            output.append("\tMONOALPHABETIC ORDER PRESERVED : SHIFT CIPHER\n");
            output.append("\tSHIFT VALUE = "+shiftValue+"\n");
        }
        else{
            output.append("\tMONOALPHABETIC ORDER NOT PRESERVED : SUBSTITUTION CIPHER\n");
        }
                                                     
        output.append("\tALPHABET USED:\n");
        output.append("\n"+getAlphabetString()+"\n\n");
                                                
                        
        if(capitals){
            output.append("\tCAPITALS ACCEPTED\n");
        }
        else{
            output.append("\tCAPITALS NOT ACCEPTED\n");
        }
        
        if(encrypting){
            output.append("\nENCRYPTING COMPLETE ["+ciphertext+"]\n");        
        }
        else{
            output.append("\nDECRYPTION COMPLETE ["+ciphertext+"]\n");
        }            

        output.append("------------------------------------------------------------\n");        
        
        return output.toString();
    }    
    
    

/** This function shifts the passed character a number of places as denoted by 
 *  the passed integer value and returns it as a new character.
 * 
 * @param inputCharacter - [char] The character to shift
 * @param shiftValue - [int] The number of shifts to perform (right is positive)
 * @return - [char] The shifted character
 */    
    public char shiftCharacter(char inputCharacter, int shiftValue){
        int code = (int)inputCharacter;
        
        if(shiftValue < 0){
            while(shiftValue < 0){
                shiftValue = shiftValue + 26;
            }
        }
        
        char output;
        
        if(code >= 97 && code <= 122){
            int value = code - 97;
            output = (char)(((value + shiftValue) % 26) + 97);            
        }
        else if(code >= 65 && code <= 90){
            int value = code - 65;
            output = (char)(((value + shiftValue) % 26) + 65);
        }
        else{
            output = inputCharacter;
        }
        
        return output;
    }
    
/** This function randomises the characters of the alphabet and returns them in
 *  a character array.
 * 
 * @return - [char[]] An array containing a random order of letters of the alphabet
 */    
    public char[] randomiseAlphabet(){
// THE INDEXES WILL REPRESENT THE PLAINTEXT LETTERS, WHILST THE ELEMENTS AT THESE
// INDEXES ARE THE CIPHERTEXT LETTERS.

        char[] output = new char[26];
        Random R = new Random();

        for(int i = 0; i < 26; i++){
            
            int index = R.nextInt(26);
            
            if(output[index] == ' '){
                output[index] = (char)(i + 97);
            }
            else{
                i--;
            }
        }
        
        return output;
    }
    

    
/** This function returns the alphabet character array of the invoking object.
 * 
 * @return - [char[]] The alphabet array of the invoking object
 */    
    public char[] getAlphabetUsed(){
        return this.alphabet;
    }

/** This function returns the alphabet used of the invoking object, but as a 
 *  String rather than a character array.
 * 
 * @return - [String] A String containing the characters of the used alphabet
 */    
    public String getAlphabetString(){
        StringBuilder output = new StringBuilder();
        
        output.append("| ");
        
        for(int i = 0; i < 26; i++){
            output.append((char)(i + 97)+" ");
        }
        
        output.append("|\n| ");
        
        for(int i = 0; i < 26; i++){
            output.append("- ");
        }
        
        output.append("|\n| ");
        
        for(int i = 0; i < 26; i++){
            output.append(this.alphabet[i]+" ");
        }
        
        output.append("|");
        
        return output.toString();
        
    }

/** This function returns the shift value of the invoking object.
 * 
 * @return - [int] The shift value of the invoking object
 */    
    public int getShiftValue(){
        return this.shiftValue;
    }

/** This function returns the boolean value ordered of the invoking object.
 * 
 * @return - [boolean] The ordered field of the invoking object
 */    
    public boolean isOrdered(){
        return this.ordered;
    }

/** This function returns the boolean value capitals of the invoking object.
 * 
 * @return - [boolean] The invoking object's capitals field
 */    
    public boolean capitalsAllowed(){
        return this.capitals;
    }
    

    
/** This function assigns the passed character array to the invoking object's
 *  alphabet field.
 * 
 * @param input - [char[]] The new alphabet value
 */    
    public void setAlphabet(char[] input){
        
        for(int i = 0; i < input.length; i++){
            
            char X = input[i];
            
            if((int)input[i] >= 65 && (int)input[i] <= 90){
                input[i] = (char)((int)input[i]+32);
            }
            else if((int)input[i] >= 97 && (int)input[i] <= 122){
                continue;
            }
            else{
                throw new IllegalArgumentException("ERROR IN ALPHABET ASSIGNMENT, UNWANTED SYMBOL");
            }
        }
        
// THIS ENSURES THAT THE ALPHABET, WHEN ASSIGNED, IS ALWAYS LOWERCASE        
        
        this.alphabet = input;
    }

/** This function creates an alphabet in which all of the characters are shifted
 *  by the passed integer value, and assigns the invoking object's alphabet field
 *  to it, alphabetic order is preserved.
 * 
 * @param shiftValue - [int] The value with which to shift the alphabet
 */ 
    public void setAlphabet(int shiftValue){
        
        if(shiftValue < 0){
            while(shiftValue < 0){
                shiftValue = shiftValue + 26;
            }
        }
        
        char[] newAlphabet = new char[26];
        
        for(int i = 0; i < 26; i++){
            newAlphabet[i] = (char)(((i + shiftValue) % 26) + 97);
        }
        
        setAlphabet(newAlphabet);
    }
        
/** This function assigns the invoking object's shift value field to the passed
 *  integer.
 * 
 * @param input  - [int] The invoking object's shift value field
 */      
    public void setShiftValue(int input){
        this.shiftValue = input;
    }

/** This function assigns the invoking object's ordered field to the passed 
 *  boolean value.
 * 
 * @param input - [boolean] The new value for the ordered field
 */    
    public void setOrdered(boolean input){
        this.ordered = input;
    }

/** This function assigns the invoking object's capitals field to the passed 
 *  boolean value.
 * 
 * @param input - [boolean] The new value for the capitals field
 */    
    public void setCapitals(boolean input){
        this.capitals = input;
    }
    
    
}